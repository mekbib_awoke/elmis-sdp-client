package org.elmis.resolver;

import org.elmis.service.LocalServiceFactory;
import org.elmis.service.RemoteServiceFactory;
import org.elmis.service.ServiceFactory;



public class ServiceResolver {
	
	private static boolean serverAvailable = true;
	private static final ServiceResolver instance = new ServiceResolver();

	private ServiceResolver() {
	}
	/**
	 * 
	 * @return
	 */
	public static ServiceResolver getInstance(){
		return instance;
	}
	/**
	 * 
	 * @return
	 */
	public ServiceFactory getServiceFactory() {
		if (serverAvailable) {
			return new RemoteServiceFactory();
		}
		return new LocalServiceFactory();
	}
}
