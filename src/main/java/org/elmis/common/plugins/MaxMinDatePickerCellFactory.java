package org.elmis.common.plugins;

import java.time.LocalDate;

import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.util.Callback;

public class MaxMinDatePickerCellFactory implements
		Callback<DatePicker, DateCell> {

	private LocalDate minDate;
	private LocalDate maxDate;

	public MaxMinDatePickerCellFactory(LocalDate minDate, LocalDate maxDate) {
		this.minDate = minDate;
		this.maxDate = maxDate;
	}

	@Override
	public DateCell call(DatePicker param) {
		return new DateCell() {
			@Override
			public void updateItem(LocalDate item, boolean empty) {
				super.updateItem(item, empty);
				if (minDate != null) {
					if (item.isBefore(minDate)) {
						setDisable(true);
						setStyle("-fx-background-color: #EFEFEF;");
					}
				}
				if (maxDate != null) {
					if (item.isAfter(maxDate)) {
						setDisable(true);
						setStyle("-fx-background-color: #EFEFEF;");
					}
				}
			}
		};
	}
}
