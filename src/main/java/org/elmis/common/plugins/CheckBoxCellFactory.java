/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.elmis.common.plugins;

import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.util.Callback;

/**
 *
 * @author Mekbib
 * @param <Regimen> the data object from which the value of the check box will be associated to
 * @param <Boolean> the actual value
 */
public class CheckBoxCellFactory<Regimen, Boolean> implements Callback<TableColumn<Regimen, Boolean>, TableCell<Regimen, Boolean>>{
    @Override
    public TableCell<Regimen, Boolean> call(TableColumn<Regimen, Boolean> p) {
        return new CheckBoxTableCell<>();
    }
}
