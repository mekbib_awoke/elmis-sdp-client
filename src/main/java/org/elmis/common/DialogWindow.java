/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.elmis.common;

import java.io.IOException;
import java.net.URL;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import org.apache.log4j.Logger;
import org.elmis.ELMISFacility;
import org.elmis.event.DefaultDialogBehavior;

/**
 *
 * @author Mekbib
 */
public class DialogWindow extends DefaultDialogBehavior {
	
	private static final Logger log = Logger.getLogger(DialogWindow.class);
	private static FXMLLoader loader;
	private boolean okClicked = false;
	private static DialogWindow instance = new DialogWindow();

	/**
	 * Sets the stage of this dialog.
	 *
	 * @param dialogStage
	 */
	public void setDialogStage(Stage myDialogStage) {
		dialogStage = myDialogStage;
	}

	/**
	 * Returns true if the user clicked OK, false otherwise.
	 *
	 * @return
	 */
	public boolean isOkClicked() {
		return okClicked;
	}

	/**
	 * Called when the user clicks ok.
	 */
	@FXML
	private void handleOk() {
		// just make it work for now, will be replaced by a validation method
		// call
		if (true) {
			okClicked = true;
			dialogStage.close();
		}
	}

	/**
	 * Called when the user clicks cancel.
	 */
	@FXML
	private void handleCancel() {
		dialogStage.close();
	}
	
	public static FXMLLoader getLoader(){
		return loader;
	}
	
	protected DialogWindow(){}
	
	public static DialogWindow getInstance(){
		return instance;
	}

	public void openDialog(URL fxmlPath, String title, Stage parentStage) {
		
		try {
			// Load the FXML file and create a new stage for the dialog
			loader = new FXMLLoader(fxmlPath);
			AnchorPane page = (AnchorPane) loader.load();
			dialogStage = new Stage();
			dialogStage.setTitle(title);
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.toFront();
			dialogStage.setResizable(false);
			dialogStage.initOwner(parentStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);		
			
			DialogWindow dialogController = loader.getController();			
			dialogController.setDialogStage(dialogStage);
			dialogController.initClosingEventListener();
			// Show the dialog and wait until the user closes it
			dialogStage.showAndWait();

		} catch (IOException ex) {
		ex.printStackTrace();
			log.error(ex.getMessage());
		}
	}

}
