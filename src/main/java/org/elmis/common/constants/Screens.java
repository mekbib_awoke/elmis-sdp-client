/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.elmis.common.constants;

/**
 *
 * @author Mekbib
 */
public class Screens {    
    
    //register all screen id's and screen's fxml file names
    //for use by our ScreensController class
    public static String mainScreenID = "ELMISDashboard";
    public static String mainScreenFile = "/org/elmis/fx/views/fxml/ELMISDashboard.fxml";
    public static String arvDispensingScreenID = "ArvDispensingClientSearch";
    public static String arvDispensingScreenFile = "/org/elmis/fx/views/fxml/arv/dispensing/ClientSearch.fxml";
}
