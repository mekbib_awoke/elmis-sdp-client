package org.elmis.event;

import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;

/**
 * 
 * @author mawoke
 *
 */
public class DefaultDialogBehavior {
	
	protected Stage dialogStage;
	private String warningTitle = "Unsaved Pending Changes Warning";
	private String warningMessage = "Are you sure you want to close without saving changes?";
	
	protected boolean hasPendingChanges = false;
	
	protected DefaultDialogBehavior(){}
	
	protected void initClosingEventListener(){
		dialogStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent windowClosingEvent) {
                if (hasPendingChanges) {
                    Action response = Dialogs.create().owner(dialogStage).title(warningTitle)
                            .masthead("Confirm Dialog")
                            .message(warningMessage)
                            .showConfirm();
                    if (response == Dialog.Actions.YES) {
                        dialogStage.close();
                    }
                    if (response == Dialog.Actions.NO) {
                        windowClosingEvent.consume();
                    }
                }
            }
        });
	}
	
	
	/**
	 * sets {@link #warningTitle}
	 * @param warningTitle
	 */
	protected void setWarningTitle(String warningTitle){
		this.warningTitle = warningTitle;
	}
	
	
	/**
	 * sets {@link #warningMessage}
	 * @param warningMessage
	 */
	protected void setWarningMessage(String warningMessage){
		this.warningMessage = warningMessage;
	}
	
	
	/**
	 * sets {@link #hasPendingChanges}
	 * @param hasPendingChanges
	 */
	protected void setPendingChanges(boolean hasPendingChanges){
		this.hasPendingChanges = hasPendingChanges;
	}
	
	
	/**
	 * 
	 * @return returns {@link #hasPendingChanges}
	 */
	protected boolean hasPendingChanges(){
		return hasPendingChanges;
	}
}
