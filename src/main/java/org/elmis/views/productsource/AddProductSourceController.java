package org.elmis.views.productsource;

import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.util.StringConverter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.StringConverter;

import org.controlsfx.dialog.Dialogs;
import org.elmis.ELMISFacility;
import org.elmis.common.DialogWindow;
import org.elmis.common.validation.ValidationUtil;
import org.elmis.facility.domain.model.ProductSource;
import org.elmis.resolver.ServiceResolver;
import org.elmis.util.DateUtil;

public class AddProductSourceController extends DialogWindow implements Initializable {

	@FXML
	public Label lblFacilityCode;

	@FXML
	public Label lblSourceName;

	@FXML
	public Label lblCreatedby;

	@FXML
	public Label lbldateCreated;

	@FXML
	public TextField txtFacilityCode;

	@FXML
	public TextField txtSourceName;

	@FXML
	public TextField txtCreatedBy;

	@FXML
	public Button btnSave;

	@FXML
	private DatePicker dPickerProductSource;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// initialize product source table
		ValidationUtil.register(dPickerProductSource,
				ValidationUtil.Type.REQUIRED, "Created Date");

		txtFacilityCode.setText(System.getProperty("facilityCode"));
	}

	protected ProductSource productsource;

	/**
	 * 
	 */

	public void addCreatedDateActionHandler(ActionEvent e) {
		dPickerProductSource.setConverter(new StringConverter<LocalDate>() {
			String pattern = "yyyy-MM-dd";
			DateTimeFormatter dateFormatter = DateTimeFormatter
					.ofPattern(pattern);

			{
				dPickerProductSource.setPromptText(pattern.toLowerCase());
			}

			@Override
			public String toString(LocalDate date) {
				if (date != null) {
					return dateFormatter.format(date);
				} else {
					return "";
				}
			}

			@Override
			public LocalDate fromString(String string) {
				if (string != null && !string.isEmpty()) {
					return LocalDate.parse(string, dateFormatter);
				} else {
					return null;
				}
			}
		});

	}

	public void addproductSourceBtnActionHandler(ActionEvent e) {
		// create product source object
		int successmsg = 0;
		productsource = new ProductSource();

		productsource.setSourcename(txtSourceName.getText());
		productsource.setFacilityid(Integer.parseInt(System
				.getProperty("facilityid")));
		productsource.setCreatedby(txtCreatedBy.getText());

		if (dPickerProductSource.getValue() != null) {
			productsource.setCreateddate(DateUtil
					.fromLocalDate(dPickerProductSource.getValue()));
		} else {
			productsource.setCreateddate(new Date());
		}

		System.out.println();

		// validate fields
		successmsg = ServiceResolver.getServiceFactory().getProductsource()
				.insertProductSource(productsource);

		if (successmsg > -1) {
			Dialogs.create().owner(ELMISFacility.mainStage).title("Success")
					.masthead("Saving Dialog").message("product source saved")
					.showInformation();
		}

	}

}
