package org.elmis.views.store;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.function.Predicate;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

import org.apache.log4j.Logger;
import org.elmis.common.DialogWindow;
import org.elmis.event.FilterProductsListener;
import org.elmis.facade.ProductFacade;
import org.elmis.facade.StockControlCardFacade;
import org.elmis.model.Node;
import org.elmis.model.Program;
import org.elmis.model.StockControlCard;
import org.elmis.resolver.ServiceResolver;
import org.elmis.service.interfaces.ICommon;
import org.elmis.service.interfaces.IProduct;
import org.elmis.service.interfaces.IStore;
import org.elmis.service.ws.client.request.StockControlCardRequest;
import org.elmis.service.ws.client.response.FacilityApprovedProductResult;

public class StockControlCardController extends DialogWindow implements Initializable {

	private static final Logger log = Logger.getLogger(StockControlCardController.class);
	
	ICommon commonService = ServiceResolver.getInstance().getServiceFactory()
			.getCommonService();
	IProduct productService = ServiceResolver.getInstance().getServiceFactory()
			.getProductService();
	IStore storeService = ServiceResolver.getInstance().getServiceFactory()
			.getStoreService();
	
	ProductFacade productFacade = ProductFacade.getInstance();
	StockControlCardFacade stcFacade = StockControlCardFacade.getInstance();

	@FXML
	private ListView<Program> lstViewPrograms;

	@FXML
	private TabPane tabPaneStockControlCard;

	@FXML
	private Tab tabProducts;

	@FXML
	private TableView<FacilityApprovedProductResult> tblProducts;

	private ObservableList<FacilityApprovedProductResult> observableProductList;
	private FilteredList<FacilityApprovedProductResult> filteredProductList;

	@FXML
	private TextField txtSearchProduct;

	@FXML
	private TableColumn<FacilityApprovedProductResult, String> colProductCode;

	@FXML
	private TableColumn<FacilityApprovedProductResult, String> colProductName;
	
	@FXML
	private TableColumn<FacilityApprovedProductResult, String> colGeneralStrength;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		// populate the program areas
		populateProgramAreas();
		// initialize the products table
		initProductsTable();
	}

	/**
	 * populates the program areas
	 */
	private void populateProgramAreas() {
		List<Program> programCodes = commonService.getPrograms();

		lstViewPrograms.setItems(FXCollections.observableList(programCodes));
		lstViewPrograms
		.setCellFactory(new Callback<ListView<Program>, ListCell<Program>>() {

			@Override
			public ListCell<Program> call(ListView<Program> arg0) {
				ListCell<Program> cell = new ListCell<Program>() {

					@Override
					protected void updateItem(Program program,
							boolean empty) {
						super.updateItem(program, empty);
						if (program != null) {
							setText(program.getCode());
						}
					}
				};
				return cell;
			}

		});
		lstViewPrograms.getSelectionModel().selectedItemProperty()
				.addListener(new ChangeListener<Program>() {
					@Override
					public void changed(
							ObservableValue<? extends Program> observable,
							Program oldProgram, Program newProgram) {

						ArrayList<FacilityApprovedProductResult> fapList = productFacade.convertToFacilityApprovedProductResult(productService.getFacilityApprovedNodeProducts(newProgram.getCode()));

						observableProductList = FXCollections
								.observableList(fapList);

						filteredProductList = new FilteredList<>(
								observableProductList);

						// initially show all the products
						// this is very important
						filteredProductList
								.setPredicate(new Predicate<FacilityApprovedProductResult>() {
									@Override
									public boolean test(
											FacilityApprovedProductResult product) {
										return true;
									}
								});

						SortedList<FacilityApprovedProductResult> sortedData = new SortedList<>(
								filteredProductList);
						sortedData.comparatorProperty().bind(
								tblProducts.comparatorProperty());
						// initialize filter products by name/code
						// text field
						initFilterByProductName();
						// bind table data to the sorted list
						tblProducts.setItems(sortedData);
						//toggle the products tab
						tabPaneStockControlCard.getSelectionModel().select(tabProducts);
						// display the products tab
						tabProducts.setStyle("visibility : true");

					}
				});
	}

	/**
	 * initialize the products table
	 */

	private void initProductsTable() {
		colProductCode.setCellValueFactory(new PropertyValueFactory<>(
				"productCode"));
		colProductName.setCellValueFactory(new PropertyValueFactory<>(
				"productName"));

		// add selection change listener
		tblProducts.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent paramT) {
				FacilityApprovedProductResult newProduct = tblProducts
						.getSelectionModel().getSelectedItem();
				
				if(newProduct != null){
					log.debug("The selected product id is " + newProduct.getProductId());
					
					StockControlCardRequest stcRequest = new StockControlCardRequest();
//					Calendar calFrom = Calendar.getInstance();
//					calFrom.set(Calendar.DAY_OF_MONTH,
//							calFrom.get(Calendar.DAY_OF_MONTH) - 30);
//
//					Calendar calTo = Calendar.getInstance();
					//TODO
					//stcRequest.setFrom(null);
					//TODO
					//stcRequest.setTo(null);
					//TODO
					Node storeNode = new Node();
					storeNode.setId(30);
					stcRequest.setStore(storeNode);
					stcRequest.setProductCode(newProduct.getProductCode());
					List<StockControlCard> sccList = stcFacade.convertToStockControlCard(storeService.generateStockControlCard(stcRequest));
					System.out.println("The size of the stock control card " + sccList.size());
					JasperPrint print;
					try {
						Map<String, Object> parameters = new HashMap<String, Object>();
						parameters.put("primaryname", newProduct.getProductName());
						parameters.put("packsize", newProduct.getPackSize());
						//TODO
						parameters.put("facilityname", "Facility Name Placeholder");
						parameters.put("productcode", newProduct.getProductCode());
						//TODO
						parameters.put("district", "District Placeholder");
						print = JasperFillManager.fillReport("Reports/stockcontrolcard.jasper", parameters, new JRBeanCollectionDataSource(sccList));
						JasperViewer.viewReport(print, false);
					} catch (JRException e) {
						log.error(e.getMessage());						
					}

					
				}

			}

		});
	}

	/**
	 * initialize filter products search by name/code to the txtSearchProduct
	 * field
	 */
	private void initFilterByProductName() {
		txtSearchProduct.textProperty().addListener(new FilterProductsListener(filteredProductList));
	}
}
