package org.elmis.views.store;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.util.Callback;

import org.apache.log4j.Logger;
import org.elmis.ELMISFacility;
import org.elmis.common.DialogWindow;
import org.elmis.model.Program;
import org.elmis.resolver.ServiceResolver;
import org.elmis.service.interfaces.ICommon;

public class ReceiveFromController extends DialogWindow implements
		Initializable {

	private static final Logger log = Logger
			.getLogger(ReceiveFromController.class);

	ICommon commonService = ServiceResolver.getInstance().getServiceFactory()
			.getCommonService();

	@FXML
	private ListView<String> lstViewSources;

	@FXML
	private ListView<Program> lstViewPrograms;

	@FXML
	private TabPane tabReceiveItems;

	@FXML
	private Tab tabProductSources;

	@FXML
	private Tab tabPrograms;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		// populates all sources
		populateSources();
		// populates all programs
		populatePrograms();
		// populateProducts();

	}

	public void populateSources() {
		// TODO
		lstViewSources.getItems().add("MSL");
		lstViewSources.getItems().add("CHAZ");
		lstViewSources.getItems().add("LOCAL PURCHASE");
		lstViewSources.getItems().add("DCMO");
		lstViewSources.getItems().add("KIT");
		lstViewSources.getItems().add("BULK STORE");

		lstViewSources.getSelectionModel().selectedItemProperty()
				.addListener(new ChangeListener<String>() {
					public void changed(
							ObservableValue<? extends String> observable,
							String oldValue, String newValue) {
						log.info("selection changed" + newValue);
						// set the source of products before opening products
						// dialog
						ReceiveProductsController.productSource = newValue;
						tabPrograms.setStyle("visibility : true");
						tabReceiveItems.getSelectionModel().select(tabPrograms);
					}
				});
	}

	public void populatePrograms() {
		List<Program> programs = commonService.getPrograms();
		lstViewPrograms.setItems(FXCollections.observableList(programs));
		lstViewPrograms
				.setCellFactory(new Callback<ListView<Program>, ListCell<Program>>() {

					@Override
					public ListCell<Program> call(ListView<Program> arg0) {
						ListCell<Program> cell = new ListCell<Program>() {

							@Override
							protected void updateItem(Program program,
									boolean empty) {
								super.updateItem(program, empty);
								if (program != null) {
									setText(program.getCode());
								}
							}
						};
						return cell;
					}

				});
		lstViewPrograms.getSelectionModel().selectedItemProperty()
				.addListener(new ChangeListener<Program>() {
					@Override
					public void changed(
							ObservableValue<? extends Program> observable,
							Program oldProgarm, Program newProgarm) {
						log.info("Program has been selected opening products dialog "
								+ newProgarm.getCode());

						// set the program area before opening products dialog
						ReceiveProductsController.productProgramArea = newProgarm.getCode();
						DialogWindow
								.getInstance()
								.openDialog(
										ELMISFacility.class
												.getResource("views/store/ReceiveProducts.fxml"),
										"Record Product Receipts", dialogStage);
					}
				});
	}

}
