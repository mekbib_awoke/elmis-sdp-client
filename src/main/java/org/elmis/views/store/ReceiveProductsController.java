package org.elmis.views.store;

import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.function.Predicate;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

import org.apache.log4j.Logger;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;
import org.elmis.common.DialogWindow;
import org.elmis.common.IntegerTextField;
import org.elmis.common.plugins.DateEditingCell;
import org.elmis.common.plugins.NumberEditingCell;
import org.elmis.common.plugins.StringEditingCell;
import org.elmis.common.validation.ValidationUtil;
import org.elmis.event.FilterProductsListener;
import org.elmis.facade.ProductFacade;
import org.elmis.model.Node;
import org.elmis.model.TransactionProduct;
import org.elmis.resolver.ServiceResolver;
import org.elmis.service.interfaces.IProduct;
import org.elmis.service.interfaces.IStore;
import org.elmis.service.ws.client.request.ReceiveProductsRequest;
import org.elmis.service.ws.client.response.FacilityApprovedProductResult;

public class ReceiveProductsController extends DialogWindow implements Initializable {

	private static final Logger log = Logger.getLogger(ReceiveProductsController.class);
	
	private IStore storeService = ServiceResolver.getInstance().getServiceFactory().getStoreService(); 
	private IProduct productService = ServiceResolver.getInstance().getServiceFactory().getProductService();
	ProductFacade productFacade = ProductFacade.getInstance();
	public static String productProgramArea;
	public static String productSource;

	@FXML
	private VBox vboxShipmentDate;

	@FXML
	private DatePicker dPickerShipmentDate;

	@FXML
	private TextField txtDisptachNumber;

	@FXML
	private TextField txtProductSearch;

	@FXML
	private TableView<FacilityApprovedProductResult> tblProducts;

	@FXML
	private TableColumn<FacilityApprovedProductResult, String> colProductCode;

	@FXML
	private TableColumn<FacilityApprovedProductResult, String> colProductName;

	@FXML
	private TableColumn<FacilityApprovedProductResult, String> colProductGeneralStrength;

	@FXML
	private TableColumn<FacilityApprovedProductResult, Date> colProductExpiryDate;

	@FXML
	private TableColumn<FacilityApprovedProductResult, Integer> colProductQuantityReceived;

	@FXML
	private TableColumn<FacilityApprovedProductResult, String> colProductRemark;

	
	private ObservableList<FacilityApprovedProductResult> observableProductList;
	private FilteredList<FacilityApprovedProductResult> filteredProductList;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		// initialize and add custom date picker component for shipment date
		dPickerShipmentDate = new DatePicker();
		vboxShipmentDate.getChildren().add(dPickerShipmentDate);

		

		/**
		 * 
		 * MUST BE CHANGED
		 * 
		 */
		
		colProductCode.setCellValueFactory(new PropertyValueFactory<>(
				"productCode"));

		colProductName.setCellValueFactory(new PropertyValueFactory<>(
				"productName"));

		colProductGeneralStrength
				.setCellValueFactory(new PropertyValueFactory<>(
						"generalStrength"));

		colProductExpiryDate.setCellValueFactory(new PropertyValueFactory<>(
				"exipryDate"));

		colProductExpiryDate.setCellFactory(new ExpiryDateCell()
				.getCellFactory());

		colProductQuantityReceived
				.setCellValueFactory(new PropertyValueFactory<>(
						"quantityReceived"));

		colProductQuantityReceived.setCellFactory(new QuantityReceivedCell()
				.getCellFactory());

		colProductRemark.setCellValueFactory(new PropertyValueFactory<>(
				"remark"));
		colProductRemark
				.setCellFactory(new RemarkCell().getCellFactory());

		ArrayList<FacilityApprovedProductResult> fapList = productFacade.convertToFacilityApprovedProductResult(productService.getFacilityApprovedNodeProducts(productProgramArea));

		observableProductList = FXCollections
				.observableList(fapList);
		
		filteredProductList = new FilteredList<>(
				observableProductList);

		// initially show all the products
		// this is very important
		filteredProductList
				.setPredicate(new Predicate<FacilityApprovedProductResult>() {
					@Override
					public boolean test(
							FacilityApprovedProductResult product) {
						return true;
					}
				});

		SortedList<FacilityApprovedProductResult> sortedData = new SortedList<>(
				filteredProductList);
		sortedData.comparatorProperty().bind(
				tblProducts.comparatorProperty());
		
		tblProducts.setItems(sortedData);
		// initialize filter products by name/code
		// text field
		initFilterByProductName();
	}
	
	
	/**
	 * 
	 */
	private void initFilterByProductName() {
		txtProductSearch.textProperty().addListener(
				new FilterProductsListener(filteredProductList));
	}

	/**
	 * will handle saving received products to database
	 * 
	 * @param event
	 */
	@FXML
	public void handleSaveProducts(ActionEvent event) {
		Integer dispensingPointId = 0;
		if (!hasPendingChanges) {
			Dialogs myDialog = Dialogs
					.create()
					.owner(dialogStage)
					.title("Receive products")
					.masthead("Information Dialog")
					.message(
							"Quantity Received is not set on any of the products, please check!");
			myDialog.showInformation();
		} else {
			//clear the validation
			ValidationUtil.resetValidtor();
			// add validation rule for fields to be validated
			ValidationUtil.register(txtDisptachNumber, ValidationUtil.Type.NUMBER,
					"Dispatch Number");
			ValidationUtil.register(dPickerShipmentDate,
					ValidationUtil.Type.REQUIRED, "Shipment Date");
			
			if (ValidationUtil.isValid()) {
				Action response = Dialogs.create().owner(dialogStage)
						.title("Receive products").masthead("Confirm Dialog")
						.message("Saving Quantity Received Information?")
						.showConfirm();
				if (response == Dialog.Actions.YES) {
					log.info("Received Products");
					// HARD CODED *** REMOVE **********************************/
					// dispensing point
//					List<Site_Dispensing_point> dispensingPoints = ServiceResolver
//							.getServiceFactory().getFacilityService()
//							.findDispensingPoints(87);
//
//					for (Site_Dispensing_point dispensingPoint : dispensingPoints) {
//						if (dispensingPoint.getProgramarea().equals(
//								productProgramArea)) {
//							// match found break the loop
//							dispensingPointId = dispensingPoint.getId();
//							break;
//						}
//					}
					/*********************************************************/
					boolean insertSucceeded = false;
					ReceiveProductsRequest receiveProductsRequest = new ReceiveProductsRequest();
					//THE Store Node
					//HARD CODED should be removed
					Node myNode = new Node();
					myNode.setId(30);
					
					//The Transaction Products
					ArrayList<TransactionProduct> productsReceived = new ArrayList<>();
					
					TransactionProduct myReceivedProduct;					
					
					
					for (FacilityApprovedProductResult product : tblProducts
							.getItems()) {						
						if (product.getQuantity() != null) {
							myReceivedProduct = new TransactionProduct();
							myReceivedProduct.setProductId(product.getProductId());
							myReceivedProduct.setQuantity(BigDecimal.valueOf(product.getQuantity()));
							//add the transaction product to the transaction list
							productsReceived.add(myReceivedProduct);
						}
					}					
					
					receiveProductsRequest.setStore(myNode);
					receiveProductsRequest.setProductsReceived(productsReceived);
					receiveProductsRequest.setTxnDate(new Date().getTime());
					receiveProductsRequest.setUserId(null);
					
					if(receiveProductsRequest.getProductsReceived().size() > 0){
						insertSucceeded = storeService.receiveProducts(receiveProductsRequest) != -1  ? true : false;
					}
					
					// show successful insertion message
					if (insertSucceeded) {
						Dialogs myDialog = Dialogs.create().owner(dialogStage)
								.title("Receive products")
								.masthead("Information Dialog")
								.message("Products saved successfully!");
						myDialog.showInformation();
						dialogStage.close();
					}
				}
			} else {
				Dialogs myDialog = Dialogs.create().owner(dialogStage)
						.title("Receive products")
						.masthead("Information Dialog")
						.message(ValidationUtil.message());
				myDialog.showInformation();
			}

		}

	}

	/**
	 * cell editor for expiry date
	 * 
	 * @author mawoke
	 *
	 */
	class ExpiryDateCell extends DateEditingCell {
		@Override
		protected void createListenerForDatePicker() {
			datePicker.valueProperty().addListener(
					new ChangeListener<LocalDate>() {

						@Override
						public void changed(
								ObservableValue<? extends LocalDate> paramObservableValue,
								LocalDate oldDate, LocalDate newDate) {

							// update the value of the expiry received field
							tblProducts
									.getItems()
									.get(getIndex())
									.setExipryDate(
											getDateFromLocalDate(newDate));
						}

					});
		}

		/**
		 * return cell factory for expire date column, that sets the custom
		 * desired cell behavior
		 * 
		 * @return
		 */
		@Override
		public Callback getCellFactory() {
			return new Callback<TableColumn<FacilityApprovedProductResult, Date>, TableCell<FacilityApprovedProductResult, Date>>() {
				@Override
				public TableCell call(TableColumn p) {
					return new ExpiryDateCell();
				}
			};
		}

	}

	/**
	 * cell editor for quantity received
	 * 
	 * @author mawoke
	 *
	 */
	class QuantityReceivedCell extends NumberEditingCell {
		@Override
		public void createTextField() {
			// TODO Auto-generated method stub
			textField = new IntegerTextField();
			textField.setText(getString());
			textField.setMinWidth(this.getWidth() - this.getGraphicTextGap()
					* 2);
			textField.setOnKeyPressed(new EventHandler<KeyEvent>() {
				@Override
				public void handle(KeyEvent t) {
					if (t.getCode() == KeyCode.ENTER) {
						Double quantityReceived = Double.valueOf(textField
								.getText());
						commitEdit(quantityReceived);
						// update the value of the quantity received field
						tblProducts.getItems().get(getIndex())
								.setQuantity(quantityReceived);
						hasPendingChanges = true;
					} else if (t.getCode() == KeyCode.ESCAPE) {
						cancelEdit();
					}
				}
			});
		}

		/**
		 * return cell factory for quantity received column, that sets the
		 * custom desired cell behavior
		 * 
		 * @return
		 */
		@Override
		public Callback getCellFactory() {
			return new Callback<TableColumn<FacilityApprovedProductResult, Integer>, TableCell<FacilityApprovedProductResult, Integer>>() {
				@Override
				public TableCell call(TableColumn p) {
					return new QuantityReceivedCell();
				}
			};
		}
	}

	/**
	 * cell editor for remark
	 * 
	 * @author mawoke
	 *
	 */
	class RemarkCell extends StringEditingCell {

		@Override
		public void createTextField() {
			textField = new TextField(getString());
			textField.setMinWidth(this.getWidth() - this.getGraphicTextGap()
					* 2);
			textField.setOnKeyPressed(new EventHandler<KeyEvent>() {
				@Override
				public void handle(KeyEvent t) {
					if (t.getCode() == KeyCode.ENTER) {
						String remark = textField.getText();
						commitEdit(remark);
						// update the value of the quantity received field
						tblProducts.getItems().get(getIndex())
								.setRemark(remark);
					} else if (t.getCode() == KeyCode.ESCAPE) {
						cancelEdit();
					}
				}
			});
			textField.focusedProperty().addListener(
					new ChangeListener<Boolean>() {
						@Override
						public void changed(
								ObservableValue<? extends Boolean> observable,
								Boolean oldValue, Boolean newValue) {
							if (!newValue && textField != null) {
								commitEdit(textField.getText());
							}
						}
					});
		}

		/**
		 * return cell factory for remark date column, that sets the custom
		 * desired cell behavior
		 * 
		 * @return
		 */
		@Override
		public Callback getCellFactory() {
			return new Callback<TableColumn<FacilityApprovedProductResult, String>, TableCell<FacilityApprovedProductResult, String>>() {
				@Override
				public TableCell call(TableColumn p) {
					return new RemarkCell();
				}
			};
		}

	}

	/**
	 * closes the receive products dialog
	 * 
	 * @param closeEvent
	 */
	public void closeProductsWindow(ActionEvent closeEvent) {
		if (hasPendingChanges) {
			Action response = Dialogs
					.create()
					.owner(dialogStage)
					.title("Receive products")
					.masthead("Confirm Dialog")
					.message(
							"There are changes that you haven't saved. Do you want to close without saving changes?")
					.showConfirm();
			if (response == Dialog.Actions.YES) {
				this.dialogStage.close();
			}
		}

	}

}
