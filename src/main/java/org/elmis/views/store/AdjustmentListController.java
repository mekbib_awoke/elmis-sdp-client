/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.elmis.views.store;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.function.Predicate;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import org.apache.log4j.Logger;
import org.elmis.ELMISFacility;
import org.elmis.common.DialogWindow;
import org.elmis.event.FilterProductsListener;
import org.elmis.facade.ProductFacade;
import org.elmis.service.ws.client.response.FacilityApprovedProductResult;

/**
 * FXML Controller class
 *
 * @author Mekbib
 */
public class AdjustmentListController extends DialogWindow implements Initializable {
	
	private static final Logger log = Logger.getLogger(AdjustmentListController.class);

	// vertical box to contain adjustment date
	@FXML
	private VBox vboxAdjDate;

	private DatePicker datePickerAdjDate;

	@FXML
	private ComboBox<String> cmbProgramCodes;

	@FXML
	private TextField txtProductName;

	@FXML
	private TableView<FacilityApprovedProductResult> tblProductList;

	// Set default program area to ARV

	private String programCode = "ARV";

	private ObservableList<FacilityApprovedProductResult> observableProductList;

	private FilteredList<FacilityApprovedProductResult> filteredProductList;

	@FXML
	private TableColumn<FacilityApprovedProductResult, String> colProductCode;

	@FXML
	private TableColumn<FacilityApprovedProductResult, String> colProductName;

	@FXML
	private TableColumn<FacilityApprovedProductResult, String> colGenericStrength;

	/**
	 * Initializes the controller class.
	 *
	 * @param url
	 * @param rb
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		datePickerAdjDate = new DatePicker();
		datePickerAdjDate.setMaxDate(new Date());
		vboxAdjDate.getChildren().add(datePickerAdjDate);

		// populate program areas
		populateProgramAreas();
		// initialize products table
		initProductsTable();
		

	}
	
	
	
	
	/**
	 * 
	 */
	private void populateProgramAreas(){
		cmbProgramCodes.getItems().add("ARV");
		cmbProgramCodes.getItems().add("HIV");		
		cmbProgramCodes.getItems().add("EM");
		cmbProgramCodes.getItems().add("LAB");
		cmbProgramCodes.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>(){

		@Override
		public void changed(
				ObservableValue<? extends String> paramObservableValue,
				String oldValue, String newValue) {
			programCode = newValue;
			initProductsTable();
		}});
		
		
	}
	
	
	
	/**
	 * 
	 */
	private void initProductsTable(){
		colProductCode.setCellValueFactory(new PropertyValueFactory<>(
				"productCode"));
		colProductName.setCellValueFactory(new PropertyValueFactory<>(
				"productName"));
		colGenericStrength.setCellValueFactory(new PropertyValueFactory<>(
				"generalStrength"));

		ArrayList<FacilityApprovedProductResult> fapList = ProductFacade.getInstance().getFacilityApprovedProducts(programCode, ProductFacade.getInstance().getCurrentFacilityTypeCode());

		observableProductList = FXCollections.observableList(fapList);

		filteredProductList = new FilteredList<>(observableProductList);

		// initially show all the products
		// this is very important
		filteredProductList
				.setPredicate(new Predicate<FacilityApprovedProductResult>() {
					@Override
					public boolean test(FacilityApprovedProductResult product) {
						return true;
					}
				});

		SortedList<FacilityApprovedProductResult> sortedData = new SortedList<>(
				filteredProductList);
		sortedData.comparatorProperty().bind(
				tblProductList.comparatorProperty());
		// initialize filter products by name/code
		// text field
		initFilterByProductName();
		// bind table data to the sorted list
		tblProductList.setItems(sortedData);

		addClickListenerToProductList();
	}
	
	
	
	
	/**
	 * 
	 */
	private void addClickListenerToProductList() {
		tblProductList.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				try {

					FacilityApprovedProductResult selectedProduct = tblProductList
							.getSelectionModel().getSelectedItem();
					
					Double systemCalculatedBalance = ProductFacade.getInstance().getSystemCalaculateBalance(selectedProduct);
					selectedProduct.setQuantity(systemCalculatedBalance);
					
					// Load the fxml file and create a new stage for the popup
					FXMLLoader loader = new FXMLLoader(ELMISFacility.class
							.getResource("views/store/RecordAdjustment.fxml"));
					AnchorPane page = (AnchorPane) loader.load();
					Stage dialogStage = new Stage();
					dialogStage.setTitle("Record ARV Dispensary Adjustments");
					dialogStage.initModality(Modality.WINDOW_MODAL);
					dialogStage.setResizable(false);
					dialogStage.initOwner(ELMISFacility.mainStage);
					Scene scene = new Scene(page);
					dialogStage.setScene(scene);
					
					RecordAdjustmentController.selectedProduct = selectedProduct;
					RecordAdjustmentController.adjustmentDate = datePickerAdjDate.getValue();
					RecordAdjustmentController controller = loader.getController();
					
					log.debug(selectedProduct.toString());
					//show product name on the label before 
					//displaying the dialog
					controller.initAll();
					controller.setDialogStage(dialogStage);

					// Show the dialog and wait until the user closes it
					dialogStage.showAndWait();

					// return controller.isOkClicked();
				} catch (IOException evt) {
					evt.printStackTrace();
					// return false;
				}
			}
		});
	}
	
	
	
	/**
	 * 
	 */
	private void initFilterByProductName() {
		txtProductName.textProperty().addListener(new FilterProductsListener(filteredProductList));
	}
}
