package org.elmis.views.store;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;

import org.apache.log4j.Logger;
import org.elmis.ELMISFacility;
import org.elmis.common.DialogWindow;
import org.elmis.model.Node;
import org.elmis.model.NodeType;
import org.elmis.resolver.ServiceResolver;
import org.elmis.service.interfaces.INode;

public class IssueToController extends DialogWindow implements Initializable {
	
	private static final Logger log = Logger.getLogger(IssueToController.class);
	
	private INode nodeService = ServiceResolver.getInstance().getServiceFactory().getNodeService();

	@FXML
	private ListView<Node> lViewDispensingPoints;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		/**
		 * HARD CODE SHOULD BE REMOVED replace nodeId 87 with dynamic value
		 */
		List<Node> dipsensingPointList = nodeService.getNodeOfType(NodeType.DISPENSING_POINT);
		
		// set the dispensing points
		lViewDispensingPoints.setItems(FXCollections
				.observableList(dipsensingPointList));

		lViewDispensingPoints
				.setCellFactory(new Callback<ListView<Node>, ListCell<Node>>() {

					@Override
					public ListCell<Node> call(
							ListView<Node> arg0) {
						ListCell<Node> cell = new ListCell<Node>() {

							@Override
							protected void updateItem(Node dispensingPoint,
									boolean empty) {
								super.updateItem(dispensingPoint, empty);
								if (dispensingPoint != null) {
									setText(dispensingPoint.getName());
								}
							}
						};
						return cell;
					}

				});
		
		lViewDispensingPoints.setOnMouseClicked(new EventHandler<MouseEvent>(){

			@Override
			public void handle(MouseEvent arg0) {
				Node dispensingPoint = lViewDispensingPoints.getSelectionModel().getSelectedItem();
				log.info("Site selected " + dispensingPoint.getName() + " site id= " + dispensingPoint.getId());
				//set the nodeId before opening issuing form
				IssueProductsController.nodeId = dispensingPoint.getId();
				DialogWindow.getInstance().openDialog(ELMISFacility.class
						.getResource("views/store/IssueProducts.fxml"),
				"Issuing Form", dialogStage);				
			}
			
		});
	}

}
