/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.elmis.views.store;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.UUID;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;

import org.apache.log4j.Logger;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;
import org.elmis.ELMISFacility;
import org.elmis.common.DialogWindow;
import org.elmis.common.IntegerTextField;
import org.elmis.common.plugins.NumberEditingCell;
import org.elmis.common.plugins.StringEditingCell;
import org.elmis.event.FilterProductsListener;
import org.elmis.facade.ProductFacade;
import org.elmis.model.Elmis_Stock_Control_Card;
import org.elmis.resolver.ServiceResolver;
import org.elmis.service.ws.client.response.FacilityApprovedProductResult;

/**
 * FXML Controller class
 *
 * @author Mekbib
 */
public class PhysicalCountController extends DialogWindow implements Initializable {

	private static final Logger log = Logger.getLogger(PhysicalCountController.class);

	@FXML
	private VBox vboxPhyCountDate;

	@FXML
	private DatePicker dPickerPhyCountDate;

	@FXML
	private TextField txtProductSearch;

	@FXML
	private ComboBox<String> cmbProgramArea;

	@FXML
	private TableView<FacilityApprovedProductResult> tblFacilityApprovedProducts;

	@FXML
	private TableColumn<FacilityApprovedProductResult, String> colProductCode;

	@FXML
	private TableColumn<FacilityApprovedProductResult, String> colProductName;

	@FXML
	private TableColumn<FacilityApprovedProductResult, String> colProductGeneralStrength;

	@FXML
	private TableColumn<FacilityApprovedProductResult, Integer> colPhysicalCount;

	@FXML
	private TableColumn<FacilityApprovedProductResult, String> colProductRemark;

	private ObservableList<FacilityApprovedProductResult> observableProductList;
	private FilteredList<FacilityApprovedProductResult> filteredProductList;

	/**
	 * Initializes the controller class.
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {

		dPickerPhyCountDate = new DatePicker();
		dPickerPhyCountDate.setMaxDate(new Date());
		vboxPhyCountDate.getChildren().add(dPickerPhyCountDate);

		// populate program areas
		populateProgramAreas();
		// initialize products table
		initProductsTable();
		// initialize filter products by name/code
		// text field
		initFilterByProductName();

	}

	/**
	 * 
	 */
	private void initProductsTable() {
		colProductCode.setCellValueFactory(new PropertyValueFactory<>(
				"productCode"));

		colProductName.setCellValueFactory(new PropertyValueFactory<>(
				"productName"));

		colProductGeneralStrength
				.setCellValueFactory(new PropertyValueFactory<>(
						"generalStrength"));

		colPhysicalCount.setCellValueFactory(new PropertyValueFactory<>(
				"quantityReceived"));

		colPhysicalCount.setCellFactory(new PhysicalCountCell()
				.getCellFactory());

		colProductRemark.setCellValueFactory(new PropertyValueFactory<>(
				"remark"));
		colProductRemark.setCellFactory(new RemarkCell().getCellFactory());

		// populate default table view with ARV products
		populateProductTable("ARV");

	}

	/**
	 * populates Program Areas
	 */
	private void populateProgramAreas() {

		cmbProgramArea.getItems().add("ARV");
		cmbProgramArea.getItems().add("HIV");
		cmbProgramArea.getItems().add("EM");
		cmbProgramArea.getItems().add("LAB");

		cmbProgramArea.getSelectionModel().selectedItemProperty()
				.addListener(new ChangeListener<String>() {
					public void changed(
							ObservableValue<? extends String> observable,
							String oldProgramCode, String newProgramCode) {

						populateProductTable(newProgramCode);

					}
				});
	}

	/**
	 * populates {@link #tblApprovedFacilityProducts} with products under the
	 * selected programCode
	 * 
	 * @param programCode
	 */
	private void populateProductTable(String programCode) {
		// this will create a sorted list for according to the
		// parameters
		ArrayList<FacilityApprovedProductResult> fapList = ProductFacade
				.getInstance().getFacilityApprovedProducts(programCode,
						ProductFacade.getInstance().getCurrentFacilityTypeCode());

		observableProductList = FXCollections.observableList(fapList);

		filteredProductList = new FilteredList<>(observableProductList);

		SortedList<FacilityApprovedProductResult> sortedData = ProductFacade
				.getInstance().getSortedApprovedProductsList(
						filteredProductList, tblFacilityApprovedProducts);
		// bind table data to the sorted list
		tblFacilityApprovedProducts.setItems(sortedData);
	}

	/**
	 * 
	 */
	private void initFilterByProductName() {
		txtProductSearch.textProperty().addListener(
				new FilterProductsListener(filteredProductList));
	}

	/**
	 * 
	 * @param event
	 */
	@FXML
	public void handleSavePhysicalCount(ActionEvent event) {
		if (hasPendingChanges) {
			savePhysicalCount();
		}
	}

	/**
	 * 
	 * @param event
	 */
	@FXML
	public void handleCancelPhysicalCount(ActionEvent event) {
		if (hasPendingChanges) {
			savePhysicalCount();			
		}
	}

	/**
	 * 
	 */
	@Override
	protected void initClosingEventListener() {
		
		dialogStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent windowClosingEvent) {
				if (hasPendingChanges) {
					savePhysicalCount();
				}
			}
		});
	}

	/**
	 * this method will check if there has been changes on the physical count
	 * and saves them to the database
	 */
	private void savePhysicalCount() {
		Dialogs myDialog = Dialogs.create().owner(dialogStage)
				.title("Store Physical Count");
		boolean succeeded = false;
		for (FacilityApprovedProductResult product : tblFacilityApprovedProducts
				.getItems()) {
			if(product.getQuantity() == null){
				product.setQuantity(0.0);
			}
			if (product.getQuantity() > 0.0) {
				// insert physical count record
				Elmis_Stock_Control_Card stcPhysicalCount = product
						.getStockControlCardObj();
				stcPhysicalCount.setId(UUID.randomUUID().toString());
				stcPhysicalCount.setIssueto_receivedfrom("Physical Count");
				stcPhysicalCount.setQty_received(0);
				stcPhysicalCount.setBalance(product.getQuantity());
				if (dPickerPhyCountDate.getValue() != null) {
					stcPhysicalCount.setCreateddate(dPickerPhyCountDate
							.getValue());
				} else {
					stcPhysicalCount.setCreateddate(new Date());
				}
				// HARD CODED SHOULD BE REMOVED
				stcPhysicalCount.setCreatedby("Mekbib Awoke");
				try {
					ServiceResolver.getServiceFactory().getFacilityService()
							.InsertstockcontrolcardProducts(stcPhysicalCount);
					succeeded = true;
				} catch (Exception ex) {
					succeeded = false;
					log.error(ex.getMessage());
				}
			}
		}

		if (succeeded) {
			hasPendingChanges = false;
			myDialog.message("Physical Count saved Successfully!");
			myDialog.showInformation();
			dialogStage.close();
		} else {
			myDialog.message("An Error has occurred while trying to save Physical Count, please try again!");
			myDialog.showError();
		}
	}

	/**
	 * 
	 * @author mawoke
	 *
	 */
	class PhysicalCountCell extends NumberEditingCell {

		@Override
		public void createTextField() {
			// TODO Auto-generated method stub
			textField = new IntegerTextField();
			textField.setText(getString());
			textField.setMinWidth(this.getWidth() - this.getGraphicTextGap()
					* 2);
			textField.setOnKeyPressed(new EventHandler<KeyEvent>() {
				@Override
				public void handle(KeyEvent t) {
					if (t.getCode() == KeyCode.ENTER) {
						Double enteredQuantity = Double.valueOf(textField
								.getText());

						FacilityApprovedProductResult currentProduct = tblFacilityApprovedProducts
								.getItems().get(getIndex());

						Double systemCalculatedBalance = ProductFacade
								.getInstance().getSystemCalaculateBalance(
										currentProduct);
						currentProduct.setQuantity(systemCalculatedBalance);
						Double adjustment = 0.0;
						// update the value of the quantity received field
						if (!systemCalculatedBalance.equals(enteredQuantity)) {

							adjustment = enteredQuantity
									- systemCalculatedBalance;

							Action response = Dialogs
									.create()
									.owner(ELMISFacility.mainStage)
									.title("Adjustment is Required")
									.masthead("Confirm Dialog")
									.message(
											"Physical count doesn't match system calculated balance."
													+ "\n\n   System calculated balance = "
													+ systemCalculatedBalance
													+ "\n   Computerized Adjustment = "
													+ adjustment
													+ " \n\nDo you want to do an Adjustment? ")
									.showConfirm();
							if (response == Dialog.Actions.YES) {
								try {
									// Load the fxml file and create a new stage
									// for the pop up
									FXMLLoader loader = new FXMLLoader(
											ELMISFacility.class
													.getResource("views/store/RecordAdjustment.fxml"));
									AnchorPane page = (AnchorPane) loader
											.load();
									Stage myDialogStage = new Stage();
									myDialogStage
											.setTitle("Record ARV Dispensary Adjustments");
									myDialogStage
											.initModality(Modality.APPLICATION_MODAL);
									myDialogStage.setResizable(false);
									myDialogStage
											.initOwner(dialogStage);
									Scene scene = new Scene(page);
									myDialogStage.setScene(scene);
									myDialogStage.toFront();

									RecordAdjustmentController.adjustmentFromPhyCount = adjustment;
									RecordAdjustmentController.selectedProduct = currentProduct;
									RecordAdjustmentController.adjustmentDate = dPickerPhyCountDate
											.getValue();
									RecordAdjustmentController controller = loader
											.getController();

									log.debug(currentProduct.toString());
									// show product name on the label before
									// displaying the dialog
									controller.initAll();
									controller.setDialogStage(myDialogStage);

									// Show the dialog and wait until the user
									// closes it
									myDialogStage.showAndWait();
									if (RecordAdjustmentController
											.isAdjustmentSuccessful()) {
										log.debug("adj was a SUCCESS");
										currentProduct
												.setQuantity(enteredQuantity);
										commitEdit(enteredQuantity);
										hasPendingChanges = true;
									} else {
										log.debug("adj was a FAILURE");
										currentProduct.setQuantity(0.0);
										hasPendingChanges = false;
									}

								} catch (IOException ex) {
									log.error(ex.getMessage());
								}
							}

						} else {
							commitEdit(enteredQuantity);
							hasPendingChanges = true;
						}

					} else if (t.getCode() == KeyCode.ESCAPE) {
						cancelEdit();
					}
				}
			});

		}

		@Override
		public Callback getCellFactory() {
			return new Callback<TableColumn<FacilityApprovedProductResult, Integer>, TableCell<FacilityApprovedProductResult, Integer>>() {
				@Override
				public TableCell call(TableColumn p) {
					return new PhysicalCountCell();
				}
			};
		}

	}

	/**
	 * 
	 * @author mawoke
	 *
	 */
	class RemarkCell extends StringEditingCell {

		@Override
		public void createTextField() {
			textField = new TextField(getString());
			textField.setMinWidth(this.getWidth() - this.getGraphicTextGap()
					* 2);
			textField.setOnKeyPressed(new EventHandler<KeyEvent>() {
				@Override
				public void handle(KeyEvent t) {
					if (t.getCode() == KeyCode.ENTER) {
						String remark = textField.getText();
						commitEdit(remark);
						// update the value of the quantity received field
						tblFacilityApprovedProducts.getItems().get(getIndex())
								.setRemark(remark);
					} else if (t.getCode() == KeyCode.ESCAPE) {
						cancelEdit();
					}
				}
			});
			textField.focusedProperty().addListener(
					new ChangeListener<Boolean>() {
						@Override
						public void changed(
								ObservableValue<? extends Boolean> observable,
								Boolean oldValue, Boolean newValue) {
							if (!newValue && textField != null) {
								commitEdit(textField.getText());
							}
						}
					});
		}

		/**
		 * return cell factory for remark date column, that sets the custom
		 * desired cell behavior
		 * 
		 * @return
		 */
		@Override
		public Callback getCellFactory() {
			return new Callback<TableColumn<FacilityApprovedProductResult, String>, TableCell<FacilityApprovedProductResult, String>>() {
				@Override
				public TableCell call(TableColumn p) {
					return new RemarkCell();
				}
			};
		}

	}
}
