/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.elmis.views.arv;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

import org.elmis.common.DialogWindow;
import org.thehecklers.dialogfx.DialogFX;

/**
 * FXML Controller class
 *
 * @author Mekbib
 */
public class InitiateRegimenController extends DialogWindow implements Initializable {

    @FXML
    TextField txtARTNumber;

    @FXML
    ComboBox cmbRegimenCategory;

    @FXML
    ComboBox cmbRegimen;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cmbRegimenCategory.getItems().addAll( "Adults Second Line - (2004 STGs)", "Adults Second Line - (2004 STGs)");
        cmbRegimenCategory.getSelectionModel().select(0);
        
        cmbRegimen.getItems().addAll( "TDF + FTC + LPV/r", "TDF + FTC + LPV/r");
        cmbRegimen.getSelectionModel().select(0);
    }

    @FXML
    public void saveRegimen(ActionEvent saveRegimenEvt) {
        DialogFX dialog = new DialogFX(DialogFX.Type.INFO);
        dialog.setTitleText("Client information");
        dialog.setMessage("Client information updated successfully!");
        dialog.showDialog();
        dialogStage.close();
    }
    
        
}
