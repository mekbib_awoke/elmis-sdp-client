package org.elmis.model;

public class TransitiveTransactionNode extends Node {
	
	public final static String SUPPLIER = "SUPPLIER";
	public final static String RECIPIENT = "RECIPIENT";
	
	private String nodeRoleInTxn = null;
	
	public String getNodeRoleInTxn() {
		return nodeRoleInTxn;
	}
	
	public void setNodeRoleInTxn(String nodeRoleInTxn) {
		this.nodeRoleInTxn = nodeRoleInTxn;
	}
}
