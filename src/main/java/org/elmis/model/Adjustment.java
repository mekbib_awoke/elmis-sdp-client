package org.elmis.model;

public class Adjustment {
    private Integer id;

    private Integer nodeProductId;

    private Integer transactionId;

    private String adjustmentType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNodeProductId() {
        return nodeProductId;
    }

    public void setNodeProductId(Integer nodeProductId) {
        this.nodeProductId = nodeProductId;
    }

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    public String getAdjustmentType() {
        return adjustmentType;
    }

    public void setAdjustmentType(String adjustmentType) {
        this.adjustmentType = adjustmentType == null ? null : adjustmentType.trim();
    }
}