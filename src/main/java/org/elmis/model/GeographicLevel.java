package org.elmis.model;

import java.util.Date;

public class GeographicLevel {
    private Integer id;

    private String code;

    private String name;

    private Integer levelnumber;

    private Date createddate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getLevelnumber() {
        return levelnumber;
    }

    public void setLevelnumber(Integer levelnumber) {
        this.levelnumber = levelnumber;
    }

    public Date getCreateddate() {
        return createddate;
    }

    public void setCreateddate(Date createddate) {
        this.createddate = createddate;
    }
}