package org.elmis.model;

public class TransactionType {
	
	public static final String POSITIVE_ADJUSTMENT = "POSITIVE_ADJUSTMENT";
	public static final String NEGATIVE_ADJUSTMENT = "NEGATIVE_ADJUSTMENT";
	public static final String ARV_DISPENSING = "ARV_DISPENSING";
	public static final String HIV_TESTING = "HIV_TESTING";
	public static final String RECEIVIING = "RECEIVING";
	public static final String ISSUING = "ISSUING";
	public static final String TRANSFER = "TRANSFER";
	public static final String RETURN = "RETURN";
	
    private Integer id;

    private String name;

    private String description;

    private Boolean transitive;

    private Integer complements;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public Boolean getTransitive() {
        return transitive;
    }

    public void setTransitive(Boolean transitive) {
        this.transitive = transitive;
    }

    public Integer getComplements() {
        return complements;
    }

    public void setComplements(Integer complements) {
        this.complements = complements;
    }

	public Boolean isPositive() {
		
		if(name.equals(ARV_DISPENSING) || name.equals(HIV_TESTING) || name.equals(NEGATIVE_ADJUSTMENT))
			return false;
		else if (name.equals(RECEIVIING) || name.equals(POSITIVE_ADJUSTMENT))
			return true;
		else 
			return null;
		
	}
}