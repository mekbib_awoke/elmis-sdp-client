package org.elmis.model;

import java.math.BigDecimal;
import java.util.Date;

public class PhysicalCount {
    private Integer id;

    private Integer nodeProductId;

    private BigDecimal expectedQuantity;

    private BigDecimal countedQuantity;

    private BigDecimal difference;

    private Date date;

    private Integer userId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNodeProductId() {
        return nodeProductId;
    }

    public void setNodeProductId(Integer nodeProductId) {
        this.nodeProductId = nodeProductId;
    }

    public BigDecimal getExpectedQuantity() {
        return expectedQuantity;
    }

    public void setExpectedQuantity(BigDecimal expectedQuantity) {
        this.expectedQuantity = expectedQuantity;
    }

    public BigDecimal getCountedQuantity() {
        return countedQuantity;
    }

    public void setCountedQuantity(BigDecimal countedQuantity) {
        this.countedQuantity = countedQuantity;
    }

    public BigDecimal getDifference() {
        return difference;
    }

    public void setDifference(BigDecimal difference) {
        this.difference = difference;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}