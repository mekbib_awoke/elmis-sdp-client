package org.elmis.model;

public class RegimenProduct {
    private Integer id;

    private Integer productid;

    private Integer regimenid;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductid() {
        return productid;
    }

    public void setProductid(Integer productid) {
        this.productid = productid;
    }

    public Integer getRegimenid() {
        return regimenid;
    }

    public void setRegimenid(Integer regimenid) {
        this.regimenid = regimenid;
    }
}