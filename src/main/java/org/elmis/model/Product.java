package org.elmis.model;

import java.math.BigDecimal;
import java.util.Date;

public class Product {
    private Integer id;

    private String code;

    private String alternateitemcode;

    private String manufacturer;

    private String manufacturercode;

    private String manufacturerbarcode;

    private String mohbarcode;

    private String gtin;

    private String type;

    private Integer displayorder;

    private String primaryname;

    private String fullname;

    private String genericname;

    private String alternatename;

    private String description;

    private String strength;

    private Integer formid;

    private Integer dosageunitid;

    private Integer categoryid;

    private Integer productgroupid;

    private String dispensingunit;

    private Short dosesperdispensingunit;

    private Short packsize;

    private Short alternatepacksize;

    private Boolean storerefrigerated;

    private Boolean storeroomtemperature;

    private Boolean hazardous;

    private Boolean flammable;

    private Boolean controlledsubstance;

    private Boolean lightsensitive;

    private Boolean approvedbywho;

    private BigDecimal contraceptivecyp;

    private BigDecimal packlength;

    private BigDecimal packwidth;

    private BigDecimal packheight;

    private BigDecimal packweight;

    private Short packspercarton;

    private BigDecimal cartonlength;

    private BigDecimal cartonwidth;

    private BigDecimal cartonheight;

    private Short cartonsperpallet;

    private Short expectedshelflife;

    private String specialstorageinstructions;

    private String specialtransportinstructions;

    private Boolean active;

    private Boolean fullsupply;

    private Boolean tracer;

    private Boolean roundtozero;

    private Boolean archived;

    private Short packroundingthreshold;

    private Integer createdby;

    private Date createddate;

    private Integer modifiedby;

    private Date modifieddate;

    private Integer qtyonhand;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    public String getAlternateitemcode() {
        return alternateitemcode;
    }

    public void setAlternateitemcode(String alternateitemcode) {
        this.alternateitemcode = alternateitemcode == null ? null : alternateitemcode.trim();
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer == null ? null : manufacturer.trim();
    }

    public String getManufacturercode() {
        return manufacturercode;
    }

    public void setManufacturercode(String manufacturercode) {
        this.manufacturercode = manufacturercode == null ? null : manufacturercode.trim();
    }

    public String getManufacturerbarcode() {
        return manufacturerbarcode;
    }

    public void setManufacturerbarcode(String manufacturerbarcode) {
        this.manufacturerbarcode = manufacturerbarcode == null ? null : manufacturerbarcode.trim();
    }

    public String getMohbarcode() {
        return mohbarcode;
    }

    public void setMohbarcode(String mohbarcode) {
        this.mohbarcode = mohbarcode == null ? null : mohbarcode.trim();
    }

    public String getGtin() {
        return gtin;
    }

    public void setGtin(String gtin) {
        this.gtin = gtin == null ? null : gtin.trim();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public Integer getDisplayorder() {
        return displayorder;
    }

    public void setDisplayorder(Integer displayorder) {
        this.displayorder = displayorder;
    }

    public String getPrimaryname() {
        return primaryname;
    }

    public void setPrimaryname(String primaryname) {
        this.primaryname = primaryname == null ? null : primaryname.trim();
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname == null ? null : fullname.trim();
    }

    public String getGenericname() {
        return genericname;
    }

    public void setGenericname(String genericname) {
        this.genericname = genericname == null ? null : genericname.trim();
    }

    public String getAlternatename() {
        return alternatename;
    }

    public void setAlternatename(String alternatename) {
        this.alternatename = alternatename == null ? null : alternatename.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public String getStrength() {
        return strength;
    }

    public void setStrength(String strength) {
        this.strength = strength == null ? null : strength.trim();
    }

    public Integer getFormid() {
        return formid;
    }

    public void setFormid(Integer formid) {
        this.formid = formid;
    }

    public Integer getDosageunitid() {
        return dosageunitid;
    }

    public void setDosageunitid(Integer dosageunitid) {
        this.dosageunitid = dosageunitid;
    }

    public Integer getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(Integer categoryid) {
        this.categoryid = categoryid;
    }

    public Integer getProductgroupid() {
        return productgroupid;
    }

    public void setProductgroupid(Integer productgroupid) {
        this.productgroupid = productgroupid;
    }

    public String getDispensingunit() {
        return dispensingunit;
    }

    public void setDispensingunit(String dispensingunit) {
        this.dispensingunit = dispensingunit == null ? null : dispensingunit.trim();
    }

    public Short getDosesperdispensingunit() {
        return dosesperdispensingunit;
    }

    public void setDosesperdispensingunit(Short dosesperdispensingunit) {
        this.dosesperdispensingunit = dosesperdispensingunit;
    }

    public Short getPacksize() {
        return packsize;
    }

    public void setPacksize(Short packsize) {
        this.packsize = packsize;
    }

    public Short getAlternatepacksize() {
        return alternatepacksize;
    }

    public void setAlternatepacksize(Short alternatepacksize) {
        this.alternatepacksize = alternatepacksize;
    }

    public Boolean getStorerefrigerated() {
        return storerefrigerated;
    }

    public void setStorerefrigerated(Boolean storerefrigerated) {
        this.storerefrigerated = storerefrigerated;
    }

    public Boolean getStoreroomtemperature() {
        return storeroomtemperature;
    }

    public void setStoreroomtemperature(Boolean storeroomtemperature) {
        this.storeroomtemperature = storeroomtemperature;
    }

    public Boolean getHazardous() {
        return hazardous;
    }

    public void setHazardous(Boolean hazardous) {
        this.hazardous = hazardous;
    }

    public Boolean getFlammable() {
        return flammable;
    }

    public void setFlammable(Boolean flammable) {
        this.flammable = flammable;
    }

    public Boolean getControlledsubstance() {
        return controlledsubstance;
    }

    public void setControlledsubstance(Boolean controlledsubstance) {
        this.controlledsubstance = controlledsubstance;
    }

    public Boolean getLightsensitive() {
        return lightsensitive;
    }

    public void setLightsensitive(Boolean lightsensitive) {
        this.lightsensitive = lightsensitive;
    }

    public Boolean getApprovedbywho() {
        return approvedbywho;
    }

    public void setApprovedbywho(Boolean approvedbywho) {
        this.approvedbywho = approvedbywho;
    }

    public BigDecimal getContraceptivecyp() {
        return contraceptivecyp;
    }

    public void setContraceptivecyp(BigDecimal contraceptivecyp) {
        this.contraceptivecyp = contraceptivecyp;
    }

    public BigDecimal getPacklength() {
        return packlength;
    }

    public void setPacklength(BigDecimal packlength) {
        this.packlength = packlength;
    }

    public BigDecimal getPackwidth() {
        return packwidth;
    }

    public void setPackwidth(BigDecimal packwidth) {
        this.packwidth = packwidth;
    }

    public BigDecimal getPackheight() {
        return packheight;
    }

    public void setPackheight(BigDecimal packheight) {
        this.packheight = packheight;
    }

    public BigDecimal getPackweight() {
        return packweight;
    }

    public void setPackweight(BigDecimal packweight) {
        this.packweight = packweight;
    }

    public Short getPackspercarton() {
        return packspercarton;
    }

    public void setPackspercarton(Short packspercarton) {
        this.packspercarton = packspercarton;
    }

    public BigDecimal getCartonlength() {
        return cartonlength;
    }

    public void setCartonlength(BigDecimal cartonlength) {
        this.cartonlength = cartonlength;
    }

    public BigDecimal getCartonwidth() {
        return cartonwidth;
    }

    public void setCartonwidth(BigDecimal cartonwidth) {
        this.cartonwidth = cartonwidth;
    }

    public BigDecimal getCartonheight() {
        return cartonheight;
    }

    public void setCartonheight(BigDecimal cartonheight) {
        this.cartonheight = cartonheight;
    }

    public Short getCartonsperpallet() {
        return cartonsperpallet;
    }

    public void setCartonsperpallet(Short cartonsperpallet) {
        this.cartonsperpallet = cartonsperpallet;
    }

    public Short getExpectedshelflife() {
        return expectedshelflife;
    }

    public void setExpectedshelflife(Short expectedshelflife) {
        this.expectedshelflife = expectedshelflife;
    }

    public String getSpecialstorageinstructions() {
        return specialstorageinstructions;
    }

    public void setSpecialstorageinstructions(String specialstorageinstructions) {
        this.specialstorageinstructions = specialstorageinstructions == null ? null : specialstorageinstructions.trim();
    }

    public String getSpecialtransportinstructions() {
        return specialtransportinstructions;
    }

    public void setSpecialtransportinstructions(String specialtransportinstructions) {
        this.specialtransportinstructions = specialtransportinstructions == null ? null : specialtransportinstructions.trim();
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getFullsupply() {
        return fullsupply;
    }

    public void setFullsupply(Boolean fullsupply) {
        this.fullsupply = fullsupply;
    }

    public Boolean getTracer() {
        return tracer;
    }

    public void setTracer(Boolean tracer) {
        this.tracer = tracer;
    }

    public Boolean getRoundtozero() {
        return roundtozero;
    }

    public void setRoundtozero(Boolean roundtozero) {
        this.roundtozero = roundtozero;
    }

    public Boolean getArchived() {
        return archived;
    }

    public void setArchived(Boolean archived) {
        this.archived = archived;
    }

    public Short getPackroundingthreshold() {
        return packroundingthreshold;
    }

    public void setPackroundingthreshold(Short packroundingthreshold) {
        this.packroundingthreshold = packroundingthreshold;
    }

    public Integer getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Integer createdby) {
        this.createdby = createdby;
    }

    public Date getCreateddate() {
        return createddate;
    }

    public void setCreateddate(Date createddate) {
        this.createddate = createddate;
    }

    public Integer getModifiedby() {
        return modifiedby;
    }

    public void setModifiedby(Integer modifiedby) {
        this.modifiedby = modifiedby;
    }

    public Date getModifieddate() {
        return modifieddate;
    }

    public void setModifieddate(Date modifieddate) {
        this.modifieddate = modifieddate;
    }

    public Integer getQtyonhand() {
        return qtyonhand;
    }

    public void setQtyonhand(Integer qtyonhand) {
        this.qtyonhand = qtyonhand;
    }
}