package org.elmis.model;

public class RegimenProductDosage {
    private Integer id;

    private Integer regimenproductid;

    private String code;

    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRegimenproductid() {
        return regimenproductid;
    }

    public void setRegimenproductid(Integer regimenproductid) {
        this.regimenproductid = regimenproductid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }
}