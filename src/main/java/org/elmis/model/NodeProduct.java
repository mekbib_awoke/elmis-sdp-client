package org.elmis.model;

import java.math.BigDecimal;

public class NodeProduct {
    private Integer id;

    private Integer productId;
    
    private Product product;

    private BigDecimal totalInflow;

    private BigDecimal totalOutflow;

    private BigDecimal totalAdjustments;

    private Integer latestPhysicalCountId;

    private Integer nodeId;

    private BigDecimal quantityOnHand;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public BigDecimal getTotalInflow() {
        return totalInflow;
    }

    public void setTotalInflow(BigDecimal totalInflow) {
        this.totalInflow = totalInflow;
    }

    public BigDecimal getTotalOutflow() {
        return totalOutflow;
    }

    public void setTotalOutflow(BigDecimal totalOutflow) {
        this.totalOutflow = totalOutflow;
    }

    public BigDecimal getTotalAdjustments() {
        return totalAdjustments;
    }

    public void setTotalAdjustments(BigDecimal totalAdjustments) {
        this.totalAdjustments = totalAdjustments;
    }

    public Integer getLatestPhysicalCountId() {
        return latestPhysicalCountId;
    }

    public void setLatestPhysicalCountId(Integer latestPhysicalCountId) {
        this.latestPhysicalCountId = latestPhysicalCountId;
    }

    public Integer getNodeId() {
        return nodeId;
    }

    public void setNodeId(Integer nodeId) {
        this.nodeId = nodeId;
    }

    public BigDecimal getQuantityOnHand() {
        return quantityOnHand;
    }

    public void setQuantityOnHand(BigDecimal quantityOnHand) {
        this.quantityOnHand = quantityOnHand;
    }
    
    public Product getProduct() {
		return product;
	}
    
    public void setProduct(Product product) {
		this.product = product;
	}
}