package org.elmis.model;

import java.math.BigDecimal;

public class NodeProductTransaction {
    private Integer id;

    private Integer nodeProductId;

    private Integer nodeTransactionId;

    private BigDecimal quantityBeforeTransaction;

    private BigDecimal transactionQuantity;

    private BigDecimal quantityAfterTransaction;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNodeProductId() {
        return nodeProductId;
    }

    public void setNodeProductId(Integer nodeProductId) {
        this.nodeProductId = nodeProductId;
    }

    public Integer getNodeTransactionId() {
        return nodeTransactionId;
    }

    public void setNodeTransactionId(Integer nodeTransactionId) {
        this.nodeTransactionId = nodeTransactionId;
    }

    public BigDecimal getQuantityBeforeTransaction() {
        return quantityBeforeTransaction;
    }

    public void setQuantityBeforeTransaction(BigDecimal quantityBeforeTransaction) {
        this.quantityBeforeTransaction = quantityBeforeTransaction;
    }

    public BigDecimal getTransactionQuantity() {
        return transactionQuantity;
    }

    public void setTransactionQuantity(BigDecimal transactionQuantity) {
        this.transactionQuantity = transactionQuantity;
    }

    public BigDecimal getQuantityAfterTransaction() {
        return quantityAfterTransaction;
    }

    public void setQuantityAfterTransaction(BigDecimal quantityAfterTransaction) {
        this.quantityAfterTransaction = quantityAfterTransaction;
    }
}