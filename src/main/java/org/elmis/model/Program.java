package org.elmis.model;

import java.util.Date;

public class Program {
    private Integer id;

    private String code;

    private String name;

    private String description;

    private Boolean active;

    private Boolean templateconfigured;

    private Boolean regimentemplateconfigured;

    private Boolean budgetingapplies;

    private Boolean usesdar;

    private Boolean push;

    private Integer createdby;

    private Date createddate;

    private Integer modifiedby;

    private Date modifieddate;

    private Boolean sendfeed;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getTemplateconfigured() {
        return templateconfigured;
    }

    public void setTemplateconfigured(Boolean templateconfigured) {
        this.templateconfigured = templateconfigured;
    }

    public Boolean getRegimentemplateconfigured() {
        return regimentemplateconfigured;
    }

    public void setRegimentemplateconfigured(Boolean regimentemplateconfigured) {
        this.regimentemplateconfigured = regimentemplateconfigured;
    }

    public Boolean getBudgetingapplies() {
        return budgetingapplies;
    }

    public void setBudgetingapplies(Boolean budgetingapplies) {
        this.budgetingapplies = budgetingapplies;
    }

    public Boolean getUsesdar() {
        return usesdar;
    }

    public void setUsesdar(Boolean usesdar) {
        this.usesdar = usesdar;
    }

    public Boolean getPush() {
        return push;
    }

    public void setPush(Boolean push) {
        this.push = push;
    }

    public Integer getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Integer createdby) {
        this.createdby = createdby;
    }

    public Date getCreateddate() {
        return createddate;
    }

    public void setCreateddate(Date createddate) {
        this.createddate = createddate;
    }

    public Integer getModifiedby() {
        return modifiedby;
    }

    public void setModifiedby(Integer modifiedby) {
        this.modifiedby = modifiedby;
    }

    public Date getModifieddate() {
        return modifieddate;
    }

    public void setModifieddate(Date modifieddate) {
        this.modifieddate = modifieddate;
    }

    public Boolean getSendfeed() {
        return sendfeed;
    }

    public void setSendfeed(Boolean sendfeed) {
        this.sendfeed = sendfeed;
    }
}