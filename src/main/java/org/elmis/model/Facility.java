package org.elmis.model;

import java.math.BigDecimal;
import java.util.Date;

public class Facility {
    private Integer id;

    private String code;

    private String name;

    private String description;

    private String gln;

    private String mainphone;

    private String fax;

    private String address1;

    private String address2;

    private Integer geographiczoneid;

    private Integer typeid;

    private Integer catchmentpopulation;

    private BigDecimal latitude;

    private BigDecimal longitude;

    private BigDecimal altitude;

    private Integer operatedbyid;

    private BigDecimal coldstoragegrosscapacity;

    private BigDecimal coldstoragenetcapacity;

    private Boolean suppliesothers;

    private Boolean sdp;

    private Boolean online;

    private Boolean satellite;

    private Integer parentfacilityid;

    private Boolean haselectricity;

    private Boolean haselectronicscc;

    private Boolean haselectronicdar;

    private Boolean active;

    private Date golivedate;

    private Date godowndate;

    private String comment;

    private Boolean datareportable;

    private Boolean virtualfacility;

    private Integer createdby;

    private Date createddate;

    private Integer modifiedby;

    private Date modifieddate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public String getGln() {
        return gln;
    }

    public void setGln(String gln) {
        this.gln = gln == null ? null : gln.trim();
    }

    public String getMainphone() {
        return mainphone;
    }

    public void setMainphone(String mainphone) {
        this.mainphone = mainphone == null ? null : mainphone.trim();
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax == null ? null : fax.trim();
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1 == null ? null : address1.trim();
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2 == null ? null : address2.trim();
    }

    public Integer getGeographiczoneid() {
        return geographiczoneid;
    }

    public void setGeographiczoneid(Integer geographiczoneid) {
        this.geographiczoneid = geographiczoneid;
    }

    public Integer getTypeid() {
        return typeid;
    }

    public void setTypeid(Integer typeid) {
        this.typeid = typeid;
    }

    public Integer getCatchmentpopulation() {
        return catchmentpopulation;
    }

    public void setCatchmentpopulation(Integer catchmentpopulation) {
        this.catchmentpopulation = catchmentpopulation;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public BigDecimal getAltitude() {
        return altitude;
    }

    public void setAltitude(BigDecimal altitude) {
        this.altitude = altitude;
    }

    public Integer getOperatedbyid() {
        return operatedbyid;
    }

    public void setOperatedbyid(Integer operatedbyid) {
        this.operatedbyid = operatedbyid;
    }

    public BigDecimal getColdstoragegrosscapacity() {
        return coldstoragegrosscapacity;
    }

    public void setColdstoragegrosscapacity(BigDecimal coldstoragegrosscapacity) {
        this.coldstoragegrosscapacity = coldstoragegrosscapacity;
    }

    public BigDecimal getColdstoragenetcapacity() {
        return coldstoragenetcapacity;
    }

    public void setColdstoragenetcapacity(BigDecimal coldstoragenetcapacity) {
        this.coldstoragenetcapacity = coldstoragenetcapacity;
    }

    public Boolean getSuppliesothers() {
        return suppliesothers;
    }

    public void setSuppliesothers(Boolean suppliesothers) {
        this.suppliesothers = suppliesothers;
    }

    public Boolean getSdp() {
        return sdp;
    }

    public void setSdp(Boolean sdp) {
        this.sdp = sdp;
    }

    public Boolean getOnline() {
        return online;
    }

    public void setOnline(Boolean online) {
        this.online = online;
    }

    public Boolean getSatellite() {
        return satellite;
    }

    public void setSatellite(Boolean satellite) {
        this.satellite = satellite;
    }

    public Integer getParentfacilityid() {
        return parentfacilityid;
    }

    public void setParentfacilityid(Integer parentfacilityid) {
        this.parentfacilityid = parentfacilityid;
    }

    public Boolean getHaselectricity() {
        return haselectricity;
    }

    public void setHaselectricity(Boolean haselectricity) {
        this.haselectricity = haselectricity;
    }

    public Boolean getHaselectronicscc() {
        return haselectronicscc;
    }

    public void setHaselectronicscc(Boolean haselectronicscc) {
        this.haselectronicscc = haselectronicscc;
    }

    public Boolean getHaselectronicdar() {
        return haselectronicdar;
    }

    public void setHaselectronicdar(Boolean haselectronicdar) {
        this.haselectronicdar = haselectronicdar;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Date getGolivedate() {
        return golivedate;
    }

    public void setGolivedate(Date golivedate) {
        this.golivedate = golivedate;
    }

    public Date getGodowndate() {
        return godowndate;
    }

    public void setGodowndate(Date godowndate) {
        this.godowndate = godowndate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment == null ? null : comment.trim();
    }

    public Boolean getDatareportable() {
        return datareportable;
    }

    public void setDatareportable(Boolean datareportable) {
        this.datareportable = datareportable;
    }

    public Boolean getVirtualfacility() {
        return virtualfacility;
    }

    public void setVirtualfacility(Boolean virtualfacility) {
        this.virtualfacility = virtualfacility;
    }

    public Integer getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Integer createdby) {
        this.createdby = createdby;
    }

    public Date getCreateddate() {
        return createddate;
    }

    public void setCreateddate(Date createddate) {
        this.createddate = createddate;
    }

    public Integer getModifiedby() {
        return modifiedby;
    }

    public void setModifiedby(Integer modifiedby) {
        this.modifiedby = modifiedby;
    }

    public Date getModifieddate() {
        return modifieddate;
    }

    public void setModifieddate(Date modifieddate) {
        this.modifieddate = modifieddate;
    }
}