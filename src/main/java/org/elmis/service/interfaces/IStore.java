package org.elmis.service.interfaces;

import java.util.List;

import org.elmis.model.report.StockControlCard;
import org.elmis.service.ws.client.request.IssueProductsRequest;
import org.elmis.service.ws.client.request.ReceiveProductsRequest;
import org.elmis.service.ws.client.request.StockControlCardRequest;

public interface IStore {
	public abstract Integer receiveProducts(ReceiveProductsRequest receivedProducts);
	public abstract Integer issueProducts(IssueProductsRequest issuedProducts);
	public abstract List<StockControlCard> generateStockControlCard(StockControlCardRequest request);
}