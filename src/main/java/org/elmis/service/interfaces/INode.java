package org.elmis.service.interfaces;

import java.util.List;

import org.elmis.model.Node;

public interface INode {
	public abstract List<Node> getNodeOfType(String type);
}
