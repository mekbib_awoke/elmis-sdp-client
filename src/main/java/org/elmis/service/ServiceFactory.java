package org.elmis.service;

import org.elmis.service.interfaces.ICommon;
import org.elmis.service.interfaces.INode;
import org.elmis.service.interfaces.IProduct;
import org.elmis.service.interfaces.IStore;
import org.elmis.service.interfaces.IUser;


/**
 * 
 * @author mekbib
 *
 */
public interface ServiceFactory {
	
	public abstract IStore getStoreService();

	public abstract IUser getUserService();
	
	public abstract IProduct getProductService();
	
	public abstract INode getNodeService();
	
	public abstract ICommon getCommonService();
	
}
