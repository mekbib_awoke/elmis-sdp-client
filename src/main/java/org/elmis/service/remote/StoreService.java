package org.elmis.service.remote;

import java.util.List;

import org.elmis.model.report.StockControlCard;
import org.elmis.service.interfaces.IStore;
import org.elmis.service.ws.client.WSClientStore;
import org.elmis.service.ws.client.request.IssueProductsRequest;
import org.elmis.service.ws.client.request.ReceiveProductsRequest;
import org.elmis.service.ws.client.request.StockControlCardRequest;

public class StoreService implements IStore{

	@Override
	public Integer receiveProducts(ReceiveProductsRequest receivedProducts) {
		return WSClientStore.getInstance().receiveProducts(receivedProducts);
	}

	@Override
	public Integer issueProducts(IssueProductsRequest issuedProducts) {
		return WSClientStore.getInstance().issueProducts(issuedProducts);
	}

	@Override
	public List<StockControlCard> generateStockControlCard(
			StockControlCardRequest request) {
		return WSClientStore.getInstance().generateStockControlCard(request);
	}

}
