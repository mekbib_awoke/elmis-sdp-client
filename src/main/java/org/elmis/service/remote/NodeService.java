package org.elmis.service.remote;

import java.util.List;

import org.elmis.model.Node;
import org.elmis.service.interfaces.INode;
import org.elmis.service.ws.client.WSClientNode;

public class NodeService implements INode {

	@Override
	public List<Node> getNodeOfType(String type) {
		return WSClientNode.getInstance().getNodesOfType(type);
	}

}
