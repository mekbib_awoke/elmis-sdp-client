package org.elmis.service.ws.client.response;

import java.sql.Timestamp;
import java.util.Date;

import org.elmis.model.Elmis_Stock_Control_Card;

public class FacilityApprovedProductResult {
	private Integer productId;
	private String productCode;
	private String productName;
	private String programCode;
	private String generalStrength;
	private Date exipryDate;
	private Double quantity;
	private Integer packSize;
	private String remark;
	
	

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getGeneralStrength() {
		return generalStrength;
	}

	public void setGeneralStrength(String generalStrength) {
		this.generalStrength = generalStrength;
	}

	public Date getExipryDate() {
		return exipryDate;
	}

	public void setExipryDate(Date exipryDate) {
		this.exipryDate = exipryDate;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Integer getPackSize() {
		return packSize;
	}

	public void setPackSize(Integer packSize) {
		this.packSize = packSize;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	public String getProgramCode() {
		return programCode;
	}

	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}

	public String toString() {
		return "[ productCode : " + productCode + ", productName : "
				+ productName +  ", programCode : "
						+ programCode + ", generalStrength : " + generalStrength
				+ ", expireDate : " + exipryDate + ", quantityReceived : "
				+ quantity + ", remark : " + remark + " ]";
	}

	/**
	 * returns a new object with values set from this
	 * 
	 * @return
	 */
	public Elmis_Stock_Control_Card getStockControlCardObj() {

		Elmis_Stock_Control_Card scc = new Elmis_Stock_Control_Card();

		Timestamp tExipryDate = null;
		
		
		if (!isNull(exipryDate))
			tExipryDate = new Timestamp(exipryDate.getTime());
		scc.setExpirydate(tExipryDate);
		scc.setQty_received(quantity);
		scc.setProductcode(productCode);
		scc.setProgram_area(programCode);
		scc.setPrimaryname(productName);
		scc.setRemark(remark);

		return scc;

	}

	/**
	 * checks for nulls
	 * 
	 * @param obj
	 * @return
	 */
	private boolean isNull(Object obj) {
		return obj == null;
	}
}
