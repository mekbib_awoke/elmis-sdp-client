package org.elmis.service.ws.client.request;

import java.util.Date;
import java.util.List;

import org.elmis.model.PhysicalCountProduct;

public class PhysicalCountRequest {
	
	private Integer nodeId;
	private List<PhysicalCountProduct> pcProducts;
	private Date date;
	private Integer userId;
	
	public Integer getNodeId() {
		return nodeId;
	}
	
	public void setNodeId(Integer nodeId) {
		this.nodeId = nodeId;
	}
	
	public List<PhysicalCountProduct> getPcProducts() {
		return pcProducts;
	}
	public void setPcProducts(List<PhysicalCountProduct> pcProducts) {
		this.pcProducts = pcProducts;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
}
