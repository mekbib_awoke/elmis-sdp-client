package org.elmis.service.ws.client;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;
import org.elmis.model.Program;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

public class WSClientCommon extends ClientCommon{

	private static final Logger log = Logger.getLogger(WSClientCommon.class);
	private static final WSClientCommon instance = new WSClientCommon();
	
	private WSClientCommon() {
		super();
	}

	/**
	 * 
	 * @return
	 */
	public static WSClientCommon getInstance() {
		return instance;
	}

	/**
	 * 
	 * @return
	 */
	private String getServiceBaseURL() {
		return wsConfig.getServiceURL() + ":" + wsConfig.getServicePort() + "/";
	}

	/**
	 * 
	 * @return
	 */
	private Client getWSClient() {
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING,
				Boolean.TRUE);
		return Client.create(clientConfig);
	}

	/**
	 * 
	 * @return
	 */
	public ArrayList<Program> getPrograms(){
		ArrayList<Program> programs = new ArrayList<>();
		
		try{
			webResource = getWSClient().resource(getServiceBaseURL() + wsConfig.getProgramsURL());
			response = webResource.type("application/json").accept("application/json").get(ClientResponse.class);
		}catch(Exception ex){
			log.error("Failed : " + ex.getMessage());
		}
		
		String output = response.getEntity(String.class);
		Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
            @Override
            public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                return new Date(json.getAsJsonPrimitive().getAsLong());
            }
        }).create();

		programs = gson.fromJson(output,
				new TypeToken<ArrayList<Program>>() {
				}.getType());

		return programs;
	}

	public static void main(String[] args) {
		System.out.println("Size of programs : " + getInstance().getPrograms().size());
	}
}
