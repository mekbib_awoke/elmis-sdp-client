package org.elmis.service.ws.client;

import org.elmis.util.ELMISWebServiceConfiguration;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class ClientCommon {
	
	protected static ELMISWebServiceConfiguration wsConfig = ELMISWebServiceConfiguration
			.getInstance();
	
	protected static WebResource webResource;
	
	protected static ClientResponse response;

	protected ClientCommon() {
		wsConfig.readConfig();
	}
}
