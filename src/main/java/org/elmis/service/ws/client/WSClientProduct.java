package org.elmis.service.ws.client;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.elmis.model.NodeProduct;
import org.elmis.service.ws.client.response.FacilityApprovedProductResult;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

public class WSClientProduct extends ClientCommon{

	private static final Logger log = Logger.getLogger(WSClientProduct.class);

	
	private static WSClientProduct instance = new WSClientProduct(); 

	private WSClientProduct() {
		super();
	}

	/**
	 * 
	 * @return
	 */
	public static WSClientProduct getInstance() {
		return instance;
	}

	/**
	 * 
	 * @return
	 */
	private String getServiceBaseURL() {
		return wsConfig.getServiceURL() + ":" + wsConfig.getServicePort() + "/";
	}

	/**
	 * 
	 * @return
	 */
	private Client getWSClient() {
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING,
				Boolean.TRUE);
		return Client.create(clientConfig);
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws com.sun.jersey.api.client.ClientHandlerException
	 */
	public ArrayList<FacilityApprovedProductResult> getFacilityApprovedProducts() {

		ArrayList<FacilityApprovedProductResult> facilityApprovedProducts = new ArrayList<>();

		try {
			webResource = getWSClient().resource(
					getServiceBaseURL()
							+ wsConfig.getFacilityApprovedProductsURL());

			response = webResource.accept("application/json")
					.type("application/json").get(ClientResponse.class);
		} catch (Exception ex) {
			log.error("FAILED : " + ex.getMessage());
			return null;
		}

		String output = response.getEntity(String.class);
		Gson gson = new Gson();

		facilityApprovedProducts = gson.fromJson(output,
				new TypeToken<ArrayList<FacilityApprovedProductResult>>() {
				}.getType());

		return facilityApprovedProducts;
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws com.sun.jersey.api.client.ClientHandlerException
	 */
	public ArrayList<NodeProduct> getFacilityApprovedNodeProducts(
			String programCode) {

		ArrayList<NodeProduct> nodeProducts = new ArrayList<>();
		try {
			webResource = getWSClient().resource(
					getServiceBaseURL()
							+ wsConfig.getFacilityApprovedNodeProductsURL()
							+ "?nodeId=" + 30 + "&programCode=" + programCode);

			response = webResource.accept("application/json")
					.type("application/json").get(ClientResponse.class);
		} catch (Exception ex) {
			log.error("FAILED : " + ex.getMessage());
			return null;
		}

		String output = response.getEntity(String.class);
		Gson gson = new Gson();

		nodeProducts = gson.fromJson(output,
				new TypeToken<ArrayList<NodeProduct>>() {
				}.getType());

		return nodeProducts;
	}

	public static void main(String[] args) {
		// read service URL Configurations from the properties file
		wsConfig.readConfig();

		System.out.println("No of facility approved node products returned = "
				+ instance.getFacilityApprovedNodeProducts("HIV").size());

	}
}
