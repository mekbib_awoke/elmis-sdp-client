package org.elmis.service.ws.client;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.elmis.model.Node;
import org.elmis.model.report.StockControlCard;
import org.elmis.service.ws.client.request.IssueProductsRequest;
import org.elmis.service.ws.client.request.ReceiveProductsRequest;
import org.elmis.service.ws.client.request.StockControlCardRequest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

public class WSClientStore extends ClientCommon {

	static final Logger log = Logger.getLogger(WSClientStore.class);

	static WSClientStore instance = new WSClientStore();

	private WSClientStore() {
		super();
	}

	/**
	 * 
	 * @return
	 */
	public static WSClientStore getInstance() {
		return instance;
	}

	/**
	 * 
	 * @return
	 */
	private String getServiceBaseURL() {
		return wsConfig.getServiceURL() + ":" + wsConfig.getServicePort() + "/";
	}

	/**
	 * 
	 * @return
	 */
	private Client getWSClient() {
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING,
				Boolean.TRUE);
		return Client.create(clientConfig);
	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	public Integer receiveProducts(ReceiveProductsRequest request) {

		String jsonReceivedProducts = new Gson().toJson(request);
		System.out.println("The JSON request : " + jsonReceivedProducts);
		Integer response = -1;

		try {
			webResource = getWSClient()
					.resource(
							getServiceBaseURL()
									+ wsConfig.getStoreReceiveProductsURL());
			response = webResource.type("application/json")
					.accept("application/json")
					.post(Integer.class, jsonReceivedProducts);
		} catch (Exception ex) {
			log.error("Failed : " + ex.getMessage());
		}

		return response;
	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	public Integer issueProducts(IssueProductsRequest request) {

		String jsonIssuedProducts = new Gson().toJson(request);
		System.out.println("The JSON request : " + jsonIssuedProducts);
		Integer response = -1;

		try {
			webResource = getWSClient().resource(
					getServiceBaseURL() + wsConfig.getStoreIssueProductsURL());
			response = webResource.type("application/json")
					.accept("application/json")
					.post(Integer.class, jsonIssuedProducts);
		} catch (Exception ex) {
			log.error("Failed : " + ex.getMessage());
		}

		return response;
	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	public List<StockControlCard> generateStockControlCard(
			StockControlCardRequest request) {
		Gson gson = new Gson();
		String jsonStockControlCardRequest = gson.toJson(request);
		List<StockControlCard> stcList = new ArrayList<>();
		try {
			webResource = getWSClient().resource(
					getServiceBaseURL()
							+ wsConfig.getStoreStockControlCardURL());

			response = webResource.accept("application/json")
					.type("application/json")
					.post(ClientResponse.class, jsonStockControlCardRequest);
		} catch (Exception ex) {
			log.error("FAILED : " + ex.getMessage());
			return null;
		}

		String output = response.getEntity(String.class);
		gson = new GsonBuilder().registerTypeAdapter(Date.class,
				new JsonDeserializer<Date>() {
					@Override
					public Date deserialize(JsonElement json, Type typeOfT,
							JsonDeserializationContext context)
							throws JsonParseException {
						return new Date(json.getAsJsonPrimitive().getAsLong());
					}
				}).create();
		stcList = gson.fromJson(output,
				new TypeToken<ArrayList<StockControlCard>>() {
				}.getType());

		return stcList;
	}

	public static void main(String[] args) {
		StockControlCardRequest testReq = new StockControlCardRequest();
		Calendar calFrom = Calendar.getInstance();
		calFrom.set(Calendar.DAY_OF_MONTH,
				calFrom.get(Calendar.DAY_OF_MONTH) - 30);

		Calendar calTo = Calendar.getInstance();

		testReq.setFrom(calFrom.getTimeInMillis());
		testReq.setTo(calTo.getTimeInMillis());
		Node storeNode = new Node();
		storeNode.setId(30);
		testReq.setStore(storeNode);
		testReq.setProductCode("ARV0001");
		for (StockControlCard stc : getInstance().generateStockControlCard(testReq)) {
			System.out.println("ARV0001, " + stc.getDate() + " : " + stc.getFromOrTo() + " : " + stc.getQtyIssued() + " : " + stc.getQtyReceived() + " : " + stc.getBalance());
		}
		
	}
}
