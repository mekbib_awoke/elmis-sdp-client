package org.elmis.service.ws.client;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.elmis.model.Node;
import org.elmis.model.NodeType;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

public class WSClientNode extends ClientCommon {

	static final Logger log = Logger.getLogger(WSClientNode.class);	
	static WSClientNode instance = new WSClientNode(); 

	private WSClientNode() {
		super();
	}

	/**
	 * 
	 * @return
	 */
	public static WSClientNode getInstance() {
		return instance;
	}

	/**
	 * 
	 * @return
	 */
	private String getServiceBaseURL() {
		return wsConfig.getServiceURL() + ":" + wsConfig.getServicePort() + "/";
	}

	/**
	 * 
	 * @return
	 */
	private Client getWSClient() {
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING,
				Boolean.TRUE);
		return Client.create(clientConfig);
	}

	/**
	 * 
	 * @return
	 */
	public ArrayList<Node> getNodesOfType(String type){
		ArrayList<Node> nodes = new ArrayList<>();
		try {
			webResource = getWSClient().resource(
					getServiceBaseURL()
							+ wsConfig.getNodeNodesByTypeURL()
							+ "?type="+ type);

			response = webResource.accept("application/json")
					.type("application/json").get(ClientResponse.class);
		} catch (Exception ex) {
			log.error("FAILED : " + ex.getMessage());
			return null;
		}

		String output = response.getEntity(String.class);
		System.out.println(output);
		Gson gson = new Gson();

		nodes = gson.fromJson(output,
				new TypeToken<ArrayList<Node>>() {
				}.getType());

		return nodes;
	}

	public static void main(String[] args) {
		System.out.println("Dispensing pnts : " + getInstance().getNodesOfType(NodeType.DISPENSING_POINT).size());
	}
}
