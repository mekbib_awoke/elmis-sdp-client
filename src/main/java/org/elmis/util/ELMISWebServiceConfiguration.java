package org.elmis.util;

import java.io.IOException;
import java.util.Properties;

public class ELMISWebServiceConfiguration {
	private static final ELMISWebServiceConfiguration instance = new ELMISWebServiceConfiguration();
	//***********************************************************
	// BASE SERVICE Configuration
	//***********************************************************
	private String serviceURL;
	private String servicePort;	
	//***********************************************************
	// COMMON SERVICE URLS
	//***********************************************************
	private String programsURL;
	//***********************************************************
	// STORE SERVICE URLS
	//***********************************************************
	private String storeReceiveProductsURL;
	private String storeIssueProductsURL;
	private String storeStockControlCardURL;
	//***********************************************************
	// PRODUCT SERVICE URLS
	//***********************************************************
	private String facilityApprovedProductsURL;
	private String facilityApprovedNodeProductsURL;
	//***********************************************************
	// NODE SERVICE URLS
	//***********************************************************
	private String nodeNodesByTypeURL;
	private String nodeAdjustmentURL;
	private String nodePhysicalCountURL;
	
	
	private ELMISWebServiceConfiguration() {
	}

	public static ELMISWebServiceConfiguration getInstance() {
		return instance;
	}

	//read from the file
	public void readConfig(){
		Properties props = new Properties();
		
		try {
			props.load(getClass().getResourceAsStream("/elmis-web-service.properties"));			
			
			this.serviceURL = props.getProperty("serviceURL");
			this.servicePort = props.getProperty("servicePort");
			//***********************************************************
			// COMMON SERVICE URLS
			//***********************************************************
			this.setProgramsURL(props.getProperty("common.programs"));
			//***********************************************************
			// STORE SERVICE URLS
			//***********************************************************
			this.storeReceiveProductsURL = props.getProperty("store.receive.products");
			this.storeStockControlCardURL = props.getProperty("store.stockcontrolcard");
			this.setStoreIssueProductsURL(props.getProperty("store.issue.products"));
			//***********************************************************
			// PRODUCT SERVICE URLS
			//***********************************************************
			this.facilityApprovedProductsURL = props.getProperty("product.facility.products");
			this.facilityApprovedNodeProductsURL = props.getProperty("product.facility.node.products");
			//***********************************************************
			// NODE SERVICE URLS
			//***********************************************************
			this.nodeNodesByTypeURL = props.getProperty("node.nodebytype");
			this.setNodeAdjustmentURL(props.getProperty("node.product.adjustment"));
			this.setNodePhysicalCountURL(props.getProperty("node.product.physicalcount"));
			
		} catch (IOException e) {		
			e.printStackTrace();
		}	
		
	}

	public String getServiceURL() {
		return serviceURL;
	}

	public void setServiceURL(String serviceURL) {
		this.serviceURL = serviceURL;
	}

	public String getServicePort() {
		return servicePort;
	}

	public void setServicePort(String servicePort) {
		this.servicePort = servicePort;
	}	
	
	public String getStoreReceiveProductsURL() {
		return storeReceiveProductsURL;
	}

	public void setStoreReceiveProductsURL(String storeReceiveProductsURL) {
		this.storeReceiveProductsURL = storeReceiveProductsURL;
	}

	public String getStoreStockControlCardURL() {
		return storeStockControlCardURL;
	}

	public void setStoreStockControlCardURL(String storeStockControlCardURL) {
		this.storeStockControlCardURL = storeStockControlCardURL;
	}

	public String getFacilityApprovedProductsURL() {
		return facilityApprovedProductsURL;
	}

	public void setFacilityApprovedProductsURL(
			String facilityApprovedProductsURL) {
		this.facilityApprovedProductsURL = facilityApprovedProductsURL;
	}

	public String getFacilityApprovedNodeProductsURL() {
		return facilityApprovedNodeProductsURL;
	}

	public void setFacilityApprovedNodeProductsURL(
			String facilityApprovedNodeProductsURL) {
		this.facilityApprovedNodeProductsURL = facilityApprovedNodeProductsURL;
	}

	public String getProgramsURL() {
		return programsURL;
	}

	public void setProgramsURL(String programsURL) {
		this.programsURL = programsURL;
	}

	public String getStoreIssueProductsURL() {
		return storeIssueProductsURL;
	}

	public void setStoreIssueProductsURL(String storeIssueProductsURL) {
		this.storeIssueProductsURL = storeIssueProductsURL;
	}

	public String getNodeNodesByTypeURL() {
		return nodeNodesByTypeURL;
	}

	public void setNodeNodesByTypeURL(String nodeNodesByTypeURL) {
		this.nodeNodesByTypeURL = nodeNodesByTypeURL;
	}

	public String getNodeAdjustmentURL() {
		return nodeAdjustmentURL;
	}

	public void setNodeAdjustmentURL(String nodeAdjustmentURL) {
		this.nodeAdjustmentURL = nodeAdjustmentURL;
	}

	public String getNodePhysicalCountURL() {
		return nodePhysicalCountURL;
	}

	public void setNodePhysicalCountURL(String nodePhysicalCountURL) {
		this.nodePhysicalCountURL = nodePhysicalCountURL;
	}
}
