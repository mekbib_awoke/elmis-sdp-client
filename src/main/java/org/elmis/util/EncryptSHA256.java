/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.elmis.util;

import elmishub.view.LoginController;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mekbib
 */
public class EncryptSHA256 {

    /**
     * converts the string passed in an SHA256 hash
     *
     * @param stringToHash
     * @return
     */
    public static String hashToSHA256(String stringToHash) {
        
        if(null == stringToHash){
            return null;
        }

        MessageDigest encryptPassword;

        byte[] encryptedPasswordBytes;
        StringBuilder encryptedPasswordBuilder = new StringBuilder();
        try {
            encryptPassword = MessageDigest.getInstance("SHA-256");
            encryptedPasswordBytes = encryptPassword.digest(stringToHash.getBytes("UTF-8"));
            for (int i = 0; i < encryptedPasswordBytes.length; i++) {
                encryptedPasswordBuilder.append(Integer.toString((encryptedPasswordBytes[i] & 0xff) + 0x100, 16).substring(1));
            }
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(EncryptSHA256.class.getName()).log(Level.SEVERE, null, ex);
        }

        return encryptedPasswordBuilder.toString();
    }

}
