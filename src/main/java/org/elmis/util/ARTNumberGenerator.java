/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.elmis.util;

/**
 *
 * @author mawoke
 */
public class ARTNumberGenerator {

    /**
     *
     * @param input
     */
    public static String generateARTNumer(String input) {
        StringBuilder sbArtNumber = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            sbArtNumber.append(input.charAt(i));
            if (i == 3 || i == 6 || i == 11) {
                sbArtNumber.append('-');
            }
        }
        return sbArtNumber.toString();
    }

}
