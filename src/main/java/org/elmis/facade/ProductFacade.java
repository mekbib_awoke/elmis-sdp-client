package org.elmis.facade;

import java.util.ArrayList;
import java.util.function.Predicate;

import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.scene.control.TableView;

import org.elmis.model.NodeProduct;
import org.elmis.model.ProductAdjustment;
import org.elmis.service.ws.client.response.FacilityApprovedProductResult;

public class ProductFacade {
	
	private static ProductFacade instance = new ProductFacade();

	private ProductFacade() {
	}

	public static ProductFacade getInstance() {
		if (null == instance) {
			instance = new ProductFacade();
		}
		return instance;
	}

	/**
	 * 
	 * @return
	 */
	public ArrayList<FacilityApprovedProductResult> convertToFacilityApprovedProductResult(
			ArrayList<NodeProduct> nodeProducts) {
		ArrayList<FacilityApprovedProductResult> fapList = new ArrayList<>();
		FacilityApprovedProductResult fapr = null;
		for (NodeProduct nodeProduct : nodeProducts) {

			fapr = new FacilityApprovedProductResult();

			fapr.setProductId(nodeProduct.getProductId());
			fapr.setProductCode(nodeProduct.getProduct().getCode());
			fapr.setProductName(nodeProduct.getProduct().getPrimaryname());
			fapr.setGeneralStrength(nodeProduct.getProduct().getStrength());
			fapr.setQuantity(nodeProduct.getQuantityOnHand().doubleValue());

			fapList.add(fapr);
		}
		return fapList;
	}

	
	
	/**
	 * 
	 */
	public SortedList<FacilityApprovedProductResult> getSortedApprovedProductsList(
			FilteredList<FacilityApprovedProductResult> filteredProductList,
			TableView<FacilityApprovedProductResult> tblProducts) {

		filteredProductList
				.setPredicate(new Predicate<FacilityApprovedProductResult>() {
					@Override
					public boolean test(FacilityApprovedProductResult arg0) {
						return true;
					}

				});

		SortedList<FacilityApprovedProductResult> sortedData = new SortedList<>(
				filteredProductList);

		sortedData.comparatorProperty().bind(tblProducts.comparatorProperty());

		return sortedData;
	}
	
	
	/**
	 * 
	 */
	public ArrayList<ProductAdjustment> getProductAdjustmentsBlank(){
		
		ArrayList<ProductAdjustment> productAdjustments = new ArrayList<>();
		
		
		
		return productAdjustments;		
	}

}
