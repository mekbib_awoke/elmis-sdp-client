/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.elmis;

import java.util.Map;
import java.util.Properties;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

import org.apache.log4j.Logger;
import org.elmis.screenframework.ScreensController;

/**
 *
 * @author Mekbib
 */
public class ELMISFacility extends Application {

    public static Stage mainStage;
    private static final Logger log = Logger.getLogger(ELMISFacility.class);

    @Override
    public void start(Stage stage) throws Exception {
        mainStage = stage;
        ScreensController mainContainer = new ScreensController();
       
        Properties prop = new Properties();
        
        prop.load(getClass().getResource("/screens.properties").openStream());
        
        
        //load all screens
        for (Map.Entry<Object, Object> entry : prop.entrySet()) {
            String screenId = entry.getKey().toString();
            String fxmlFilePath = entry.getValue().toString();
            mainContainer.loadScreen(screenId, fxmlFilePath);
            log.info("screen id " + screenId + " , file " + fxmlFilePath);
        }

        //set the initial screen to be the dash board
        mainContainer.setScreen("ELMISDashboard");
        Group root = new Group();
        root.getChildren().addAll(mainContainer);
        Scene scene = new Scene(root);
        //setting the window size to 900x600 pixels
        stage.setWidth(900);
        stage.setHeight(600);
        stage.setScene(scene);
        stage.setResizable(true);
        //show the root window to the user
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
